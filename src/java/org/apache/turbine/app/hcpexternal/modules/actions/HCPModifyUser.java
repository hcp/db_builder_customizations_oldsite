//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/* 
 * XDAT ï¿½ Extensible Data Archive Toolkit
 * Copyright (C) 2005 Washington University
 */
/*
 * Created on Jan 25, 2005
 *
 */
package org.apache.turbine.app.hcpexternal.modules.actions;
import java.io.IOException;
import java.util.Hashtable;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.turbine.modules.ActionLoader;
import org.apache.turbine.modules.actions.VelocityAction;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.hcp.security.HcpLdapAuthenticator;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.entities.XdatUserAuth;
import org.nrg.xdat.om.XdatUser;
import org.nrg.xdat.security.PasswordValidatorChain;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.security.XDATUser.PasswordComplexityException;
import org.nrg.xdat.turbine.modules.actions.SecureAction;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xdat.turbine.utils.PopulateItem;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.XFT;
import org.nrg.xft.event.EventDetails;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.XFTItem;
import org.nrg.xft.exception.InvalidPermissionException;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xft.utils.SaveItemHelper;
/**
 * 
 * @author Tim
 * 
 * 
 */
public class HCPModifyUser extends SecureAction {
	static Logger logger = Logger.getLogger(HCPModifyUser.class);
	public void doPerform(RunData data, Context context) throws Exception
	{
		// TurbineUtils.OutputPassedParameters(data,context,this.getClass().getName());
		// parameter specifying elementAliass and elementNames
		String header = "ELEMENT_";
		int counter = 0;
		Hashtable hash = new Hashtable();
		while (((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter(header + counter,data)) != null)
		{
			String elementToLoad = ((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter(
					header + counter++,data));
			Integer numberOfInstances = ((Integer)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedInteger(
					elementToLoad,data,null));
			if (numberOfInstances != null && numberOfInstances.intValue() != 0)
			{
				int subCount = 0;
				while (subCount != numberOfInstances.intValue())
				{
					hash.put(elementToLoad + (subCount++), elementToLoad);
				}
			} else {
				hash.put(elementToLoad, elementToLoad);
			}
		}
		
		
		PopulateItem populater = PopulateItem.Populate(data,
				org.nrg.xft.XFT.PREFIX + ":user", true);
		ItemI found = populater.getItem();
		String emailWithWhite = found.getStringProperty("email");
		if(emailWithWhite != null) {
			String noWhiteEmail = emailWithWhite.trim();
			found.setProperty("email", noWhiteEmail);
		}
		XDATUser authenticatedUser=TurbineUtils.getUser(data);
		
		String login=found.getStringProperty("login");
		if(login==null){
			notifyAdmin(authenticatedUser, data,403,"Possible Authorization Bypass event", "User attempted to modify a user account other then his/her own.  This typically requires tampering with the HTTP form submission process.");
			return;
		}
		
		XdatUser oldUser=XdatUser.getXdatUsersByLogin(login, null, false);
				
		try {
			
			ModifyUser(data, authenticatedUser, found,EventUtils.newEventInstance(EventUtils.CATEGORY.SIDE_ADMIN, EventUtils.TYPE.WEB_FORM,((oldUser==null))?"Added User "+login:"Modified User "+login));
			
		} catch (InvalidPermissionException e) {
			notifyAdmin(authenticatedUser, data,403,"Possible Authorization Bypass event", "User attempted to modify a user account other then his/her own.  This typically requires tampering with the HTTP form submission process.");
			return;
		} catch (PasswordComplexityException e){
			data.setMessage( e.getMessage());
			data.setScreenTemplate("XDATScreen_edit_xdat_user.vm");
			return;

		} catch (Exception e) {
			logger.error("Error Storing User", e);
			return;
		}
		data.getParameters().setString("search_element",
				org.nrg.xft.XFT.PREFIX + ":user");
		data.getParameters().setString("search_field",
				org.nrg.xft.XFT.PREFIX + ":user.login");
		data.getParameters().setString(
				"search_value",
				found.getProperty(
						org.nrg.xft.XFT.PREFIX + ":user" + XFT.PATH_SEPERATOR
								+ "login").toString());
		data.setAction("DisplayItemAction");
		VelocityAction action = (VelocityAction) ActionLoader.getInstance()
				.getInstance("DisplayItemAction");
		action.doPerform(data, context);
	}
	
	
	/*
	 * PULLED IN METHOD FROM XDATUser 
	 */
    public static void ModifyUser(RunData data, XDATUser authenticatedUser, ItemI found,EventDetails ci) throws InvalidPermissionException, Exception {
    	String id;
    	try {
			id=(found.getStringProperty("xdat_user_id")==null)?found.getStringProperty("login"):found.getStringProperty("xdat_user_id");
		} catch (Exception e1) {
			id=found.getStringProperty("login");
	    }
		
    	PersistentWorkflowI wrk=PersistentWorkflowUtils.getOrCreateWorkflowData(null, authenticatedUser, found.getXSIType(),id,PersistentWorkflowUtils.getExternalId(found), ci);
         
    	try {
	    	ModifyUser(data,authenticatedUser,found,wrk.buildEvent());
	    	 
	    	if(id.equals(found.getStringProperty("login"))) {
                String userId = found.getStringProperty("xdat_user_id");
                if (org.apache.commons.lang.StringUtils.isBlank(userId)) {
                    XdatUser user = XDATUser.getXdatUsersByLogin(id, null, false);
                    userId = user.getXdatUserId().toString();
                    if (org.apache.commons.lang.StringUtils.isBlank(userId)) {
                        throw new Exception("Couldn't find a user for the indicated login: " + found.getStringProperty("login"));
                    }
                }
                wrk.setId(userId);
	    	}
	    	
			PersistentWorkflowUtils.complete(wrk,wrk.buildEvent());
		} catch (Exception e) {
			PersistentWorkflowUtils.fail(wrk,wrk.buildEvent());
			throw e;
		}
    }
    
    
	/*
	 * PULLED IN METHOD FROM XDATUser 
	 */
    public static void ModifyUser(RunData data, XDATUser authenticatedUser, ItemI found,EventMetaI ci) throws InvalidPermissionException, Exception {
        ItemSearch search = new ItemSearch();
        search.setAllowMultiples(false);
        search.setElement("xdat:user");
        search.addCriteria("xdat:user.login", found.getProperty("login"));
        ItemI temp = search.exec().getFirst();
		if (temp == null) {
			
			String ldap_account =data.getRequest().getParameter("ldap_account");
			if (ldap_account!=null && ldap_account.equals("1")) {
				// LDAP User Account
				String login = found.getStringProperty("login");
				String primary_password = found.getStringProperty("primary_password");
				String email = found.getStringProperty("email");
				String firstname = found.getStringProperty("firstname");
				String lastname = found.getStringProperty("lastname");
				String institution = data.getRequest().getParameter("institution");
				String dn = HcpLdapAuthenticator.RegisterNewLdapAccount(login, email, primary_password, firstname, lastname, institution, "false", "false");
				found.setProperty("primary_password", "NULL");
				found.setProperty("primary_password_encrypt", "NULL");
				found.setProperty("quarantine_path", dn);
				
			} else {
	
				// NEW USER
			    if (authenticatedUser.checkRole("Administrator")) {
			        String tempPass = found
			                .getStringProperty("primary_password");
			        if (!StringUtils.isEmpty(tempPass)){
			        	PasswordValidatorChain validator = XDAT.getContextService().getBean(PasswordValidatorChain.class);
			        	if(validator.isValid(tempPass, null)){
			        		//this is set to null instead of authenticatedUser because new users should be able to use any password even those that have recently been used by other users.
			        		found.setProperty("primary_password", XDATUser
			                    .EncryptString(tempPass, "SHA-256"));
			        	} else {
			        		throw new PasswordComplexityException(validator.getMessage());
			        	}
			        }
			        found.setProperty(
			                "xdat:user.assigned_roles.assigned_role[0].role_name",
			                "SiteUser");
			        XDATUser newUser = new XDATUser(found);
			        // newUser.initializePermissions();
			        SaveItemHelper.authorizedSave(newUser, authenticatedUser, true, false, true, false,ci);
			        XdatUserAuth newUserAuth = new XdatUserAuth((String)found.getProperty("login"), "localdb");
			        XDAT.getXdatUserAuthService().create(newUserAuth);
			    } else {
			        throw new InvalidPermissionException("Unauthorized user modification attempt");
			    }
		    
		    
		    }
		    
		    
		} else {
		    // OLD USER
		    String tempPass = found.getStringProperty("primary_password");
		    String savedPass = temp.getStringProperty("primary_password");
		    
			if (savedPass==null || savedPass.equals("")) {
				// Update LDAP record
				HcpLdapAuthenticator.UpdateLdapAccount(data, found.getProps());
				found.setProperty("primary_password", "NULL");
				found.setProperty("primary_password_encrypt", "NULL");
				
			} else if (StringUtils.isEmpty(tempPass)
		            && StringUtils.isEmpty(savedPass)) {

		    } else if (StringUtils.isEmpty(tempPass)) {

		    } else {
		        if (!tempPass.equals(savedPass)){
		        	PasswordValidatorChain validator = XDAT.getContextService().getBean(PasswordValidatorChain.class);
		        	if(validator.isValid(tempPass, authenticatedUser)){
		            found.setProperty("primary_password", XDATUser
		                    .EncryptString(tempPass, "SHA-256"));
		        	} else {
		        		throw new PasswordComplexityException(validator.getMessage());
		        	}
		        }
		    }

		    if (authenticatedUser.checkRole("Administrator")) {
		        SaveItemHelper.authorizedSave(found, authenticatedUser, false, false,ci);
		    } else if (found.getProperty("login").equals(authenticatedUser.getLogin())) {
		        XFTItem toSave = XFTItem.NewItem("xdat:user", authenticatedUser);
		        toSave.setProperty("login", authenticatedUser.getLogin());
		        toSave.setProperty("primary_password", found.getProperty("primary_password"));
		        toSave.setProperty("email", found.getProperty("email"));
		        SaveItemHelper.authorizedSave(toSave, authenticatedUser, false, false,ci);

		        authenticatedUser.setProperty("primary_password", found.getProperty("primary_password"));
		        authenticatedUser.setProperty("email", found.getProperty("email"));
		    } else {
		        throw new InvalidPermissionException("Unauthorized user modification attempt");
		    }
		}
    }

	
}
