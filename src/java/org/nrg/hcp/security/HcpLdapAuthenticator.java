// Copyright 2012 Washington University School of Medicine All Rights Reserved
/*
 * NOTE:  This program is basically a modification (extension) of org.nrg.xnat.security.LDAPAuthenticator.  It couldn't just extend
 * HcpLdapAuthenticator because id needs access to private fields that aren't exposed by methods in the LDAPAuthenticator.  May want to
 * see if XNAT wants these enhancements (mainly configurable auto-enable and LdapRegistration), however I believe changes
 * are coming with this file in coming XNAT versions.
 */
package org.nrg.hcp.security;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.nrg.action.ClientException;
import org.nrg.config.services.ConfigService;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.entities.XDATUserDetails;
import org.nrg.xdat.om.XdatUser;
import org.nrg.xdat.om.XdatUserGroupid;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.base.BaseXnatProjectdata;
import org.nrg.xdat.schema.SchemaElement;
import org.nrg.xdat.security.Authenticator;
import org.nrg.xdat.security.UserGroup;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.security.XDATUser.ActivationException;
import org.nrg.xdat.security.XDATUser.EnabledException;
import org.nrg.xdat.security.XDATUser.PasswordAuthenticationException;
import org.nrg.xdat.security.XDATUser.UserNotFoundException;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFT;
import org.nrg.xft.XFTItem;
import org.nrg.xft.XFTTable;
import org.nrg.xft.db.PoolDBUtils;
import org.nrg.xft.db.ViewManager;
import org.nrg.xft.event.EventDetails;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.FieldNotFoundException;
import org.nrg.xft.exception.InvalidPermissionException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xft.schema.design.SchemaElementI;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.utils.WorkflowUtils;
import org.restlet.data.Status;

import com.google.common.collect.Iterators;

/**
 * The HcpLdapAuthenticator HCP-specific LDAPAuthenticator 
 * 
 * Currently just makes auto-enabling of accounts configurable
 */
public class HcpLdapAuthenticator extends Authenticator {

	static org.apache.log4j.Logger logger = Logger
			.getLogger(HcpLdapAuthenticator.class);

	//LDAP server to connect to
	private static String LDAP_HOST = "ldap://path.to.ldap.host";

	// account to use when connecting to LDAP server to verify account details
	private static String LDAP_USER = "CN=xnat-account,OU=Users,DC=your,DC=edu";
	private static String SEARCHBASE = "dc=your,dc=edu";
	private static String LDAP_PASS = "PASSWORD";

	private static String INITIAL_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
	private static String SECURITY_AUTHENTICATION = "simple";
	private static String REFERRAL = "follow";

	// search used to query for the user's distinguishedName
	private static String SEARCH_TEMPLATE = "(&(objectClass=user)(CN=%USER%))";
	private static String LDAP_USER_PK = "distinguishedName";

	public static String LDAP_TO_XNAT_PK_CACHE = "xdat:user/quarantine_path";

	public static String[] USER_CLASS = {};

	public static String[] USER_GROUPS = {};

	public static String[] WB_GROUPS = {};
	public static String[] WB_PROJECTS = {};

	public static String[] FTP_GROUPS = {};
	public static String[] FTP_PROJECTS = {};
	
	public static String[] PHASE1_GROUPS = {};
	public static String[] PHASE1_PROJECTS = {};
	
	public static String[] PHASE2_GROUPS = {};
	public static String[] PHASE2_PROJECTS = {};
	
	public static String[] NKI_GROUPS = {};
	public static String[] NKI_PROJECTS = {};

	public static int AUTHENTICATION_EXPIRATION = 3600; // seconds

	public static boolean IGNORE_MULTIPLE_MATCHES = false;
	public static boolean CREATE_MISSING_ACCOUNTS = true;

	// HCP-specific fields
	private static boolean AUTO_ENABLE_ACCOUNTS = true;
	private static String USER_CONTEXT;

	public static final String XDAT_USER_ELEMENT = "xdat:user";
	public static final String DATAUSE_ACCEPT = "Accepted Data Use Terms";
	public static final String WB_ACCEPT = "Accepted Workbench Terms";
	public static final String PHASE1_ACCEPT = "Accepted Phase I DU Terms";
	public static final String PHASE2_ACCEPT = "Accepted Phase II DU Terms";
	public static final String NKI_ACCEPT = "Accepted NKI-Rockland DU Terms";

	public final static Map<String, String> params = new HashMap<String, String>();

	private LdapContext ctx;
	private Hashtable<String, String> env;

	public static boolean ENABLED = false;

	static File AUTH_PROPS = null;

	public HcpLdapAuthenticator() {
		try {
			if (AUTH_PROPS == null) {
				
				ConfigService configService = XDAT.getConfigService();
				String config = configService.getConfigContents("ldap","groups.xml");
				// convert String into InputStream
				InputStream is = new ByteArrayInputStream(config.getBytes());
				Properties groupProps = new Properties();
				groupProps.loadFromXML(is);
				
				AUTH_PROPS = new File(XFT.GetConfDir(),
						"authentication.properties");
				if (AUTH_PROPS.exists()) {
					ENABLED = true;
				} else {
					logger.info("No authentication.properties file found in conf directory. Skipping enhanced authentication method.");
					ENABLED = false;
					return;
				}
				InputStream inputs = new FileInputStream(AUTH_PROPS);
				Properties properties = new Properties();
				properties.load(inputs);

				if (properties.containsKey("LDAP_HOST"))
					LDAP_HOST = properties.getProperty("LDAP_HOST");
				else
					throw new Exception("Missing LDAP_HOST");

				if (properties.containsKey("LDAP_USER"))
					LDAP_USER = properties.getProperty("LDAP_USER");
				else
					throw new Exception("Missing LDAP_USER");

				if (properties.containsKey("SEARCHBASE"))
					SEARCHBASE = properties.getProperty("SEARCHBASE");
				else
					throw new Exception("Missing SEARCHBASE");

				if (properties.containsKey("LDAP_PASS"))
					LDAP_PASS = properties.getProperty("LDAP_PASS");
				else
					throw new Exception("Missing LDAP_PASS");

				if (properties.containsKey("INITIAL_CONTEXT_FACTORY"))
					INITIAL_CONTEXT_FACTORY = properties
							.getProperty("INITIAL_CONTEXT_FACTORY");
				else
					throw new Exception("Missing INITIAL_CONTEXT_FACTORY");

				if (properties.containsKey("SECURITY_AUTHENTICATION"))
					SECURITY_AUTHENTICATION = properties
							.getProperty("SECURITY_AUTHENTICATION");
				else
					throw new Exception("Missing SECURITY_AUTHENTICATION");

				if (properties.containsKey("REFERRAL"))
					REFERRAL = properties.getProperty("REFERRAL");
				else
					throw new Exception("Missing REFERRAL");

				if (properties.containsKey("SEARCH_TEMPLATE"))
					SEARCH_TEMPLATE = properties.getProperty("SEARCH_TEMPLATE");
				else
					throw new Exception("Missing SEARCH_TEMPLATE");

				if (properties.containsKey("LDAP_USER_PK"))
					LDAP_USER_PK = properties.getProperty("LDAP_USER_PK");
				else
					throw new Exception("Missing LDAP_USER_PK");

				if (properties.containsKey("USER_CLASS"))
					USER_CLASS = properties.getProperty("USER_CLASS").split(
							"[;:]");

				if (properties.containsKey("IGNORE_MULTIPLE_MATCHES"))
					IGNORE_MULTIPLE_MATCHES = Boolean.parseBoolean(properties
							.getProperty("IGNORE_MULTIPLE_MATCHES"));

				if (properties.containsKey("CREATE_MISSING_ACCOUNTS"))
					CREATE_MISSING_ACCOUNTS = Boolean.parseBoolean(properties
							.getProperty("CREATE_MISSING_ACCOUNTS"));

				if (properties.containsKey("AUTHENTICATION_EXPIRATION"))
					AUTHENTICATION_EXPIRATION = Integer.parseInt(properties
							.getProperty("AUTHENTICATION_EXPIRATION"));
				else
					throw new Exception("Missing AUTHENTICATION_EXPIRATION");

				if (properties.containsKey("AUTO_ENABLE"))
					AUTO_ENABLE_ACCOUNTS = properties
							.getProperty("AUTO_ENABLE").trim()
							.equalsIgnoreCase("Y");

				params.put("CN", "xdat:user/login");
				params.put("givenName", "xdat:user/firstname");
				params.put("sn", "xdat:user/lastname");
				params.put("mail", "xdat:user/email");
				params.put(LDAP_USER_PK, LDAP_TO_XNAT_PK_CACHE);
				
				////////////////////////////////////////////////
				// GROUPS (LOADED FROM CONFIGURATION SERVICE) //
				////////////////////////////////////////////////
				
				if (groupProps.containsKey("USER_GROUPS"))
					USER_GROUPS = groupProps.getProperty("USER_GROUPS").split(
							"[;:]");

				if (groupProps.containsKey("WB_GROUPS"))
					WB_GROUPS = groupProps.getProperty("WB_GROUPS").split(
							"[;:]");

				if (groupProps.containsKey("WB_PROJECTS"))
					WB_PROJECTS = groupProps.getProperty("WB_PROJECTS").split(
							"[;:]");

				if (groupProps.containsKey("FTP_GROUPS"))
					FTP_GROUPS = groupProps.getProperty("FTP_GROUPS").split(
							"[;:]");

				if (groupProps.containsKey("FTP_PROJECTS"))
					FTP_PROJECTS = groupProps.getProperty("FTP_PROJECTS").split(
							"[;:]");

				if (groupProps.containsKey("PHASE1_GROUPS"))
					PHASE1_GROUPS = groupProps.getProperty("PHASE1_GROUPS").split(
							"[;:]");

				if (groupProps.containsKey("PHASE1_PROJECTS"))
					PHASE1_PROJECTS = groupProps.getProperty("PHASE1_PROJECTS").split(
							"[;:]");

				if (groupProps.containsKey("PHASE2_GROUPS"))
					PHASE2_GROUPS = groupProps.getProperty("PHASE2_GROUPS").split(
							"[;:]");

				if (groupProps.containsKey("PHASE2_PROJECTS"))
					PHASE2_PROJECTS = groupProps.getProperty("PHASE2_PROJECTS").split(
							"[;:]");

				if (groupProps.containsKey("NKI_GROUPS"))
					NKI_GROUPS = groupProps.getProperty("NKI_GROUPS").split(
							"[;:]");

				if (groupProps.containsKey("NKI_PROJECTS"))
					NKI_PROJECTS = groupProps.getProperty("NKI_PROJECTS").split(
							"[;:]");

				if (properties.containsKey("USER_CONTEXT"))
					USER_CONTEXT = properties.getProperty("USER_CONTEXT");
			}

		} catch (Exception e) {
			logger.info(e.getMessage());
			ENABLED = false;
			logger.error("Problem during init", e);
		}
	}

	public void closeContext() {
		try {
			if (ctx != null)
				ctx.close();
			ctx = null;
		} catch (NamingException e) {
			logger.error("Problem during close", e);

		}
	}

	public void openContext(String user, String pass) throws NamingException {
		try {
			env = new Hashtable<String, String>();
			env.put(Context.SECURITY_PRINCIPAL, user);
			env.put(Context.SECURITY_CREDENTIALS, pass);
			env.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
			env.put(Context.SECURITY_AUTHENTICATION, SECURITY_AUTHENTICATION);
			env.put(Context.PROVIDER_URL, LDAP_HOST);
			env.put(Context.SECURITY_PROTOCOL, "ssl");
			env.put(Context.REFERRAL, REFERRAL);

			// Create the initial directory context
			ctx = new InitialLdapContext(env, null);
		} catch (NamingException e) {
			throw e;
		}
	}

	/**
	 * Gets LDAP information for users based on search filter
	 * 
	 * Step 1: query the server for a list of matching users (based on search
	 * filter and submitted cred) Step 2: use the results to populate XDATUser
	 * objects
	 * 
	 * @param searchFilter
	 *            The JNDI search filter you want to use
	 * @return Array of LoginBean objects for users found.
	 */
	public XDATUser getUsers(Credentials cred) {
		return getUsers(cred.getUsername());
	}
	public XDATUser getUsers(String username) {
		logger.debug("\n\ngetUsers:" + username);
		String searchFilter = buildSearchFilter(username);
		XDATUser loginBean = null;
		ArrayList<XDATUser> users = new ArrayList<XDATUser>();
		// Create the search controls
		SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		logger.debug(params.toString());
		String[] returnedAtts = new String[params.size()];
		int c = 0;
		for (Map.Entry<String, String> e : params.entrySet()) {
			returnedAtts[c++] = e.getKey();
		}

		// String returnedAtts[] = {"displayName", "sn", "givenName",
		// "sAMAccountName", "mail", "department", "telephoneNumber", "company",
		// "operations", "memberOf"};
		searchCtls.setReturningAttributes(returnedAtts);
		try {
			this.openContext(LDAP_USER, LDAP_PASS);
			logger.debug(ctx.getEnvironment().toString());
			logger.debug("SEARCHBASE:" + SEARCHBASE);
			logger.debug("FILTER:" + searchFilter);
			logger.debug("searchCtls:" + searchCtls.toString());
			for (String s : returnedAtts) {
				logger.debug("returnedAtts:" + s);
			}
			// Search for objects using the filter
			NamingEnumeration answer = ctx.search(SEARCHBASE, searchFilter,
					searchCtls);
			int match = 0;
			// Loop through the search results
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				Attributes attrs = sr.getAttributes();
				String cn = sr.getClassName();
				logger.debug("getUsers:answer[" + match++ + "]=" + sr.getName()
						+ " " + sr.getClassName() + " " + attrs.size());

				try {
					XFTItem i = XFTItem.NewItem("xdat:user", null);

					if (attrs != null) {
						try {
							// multiple attributes returned from server need to
							// be mapped to a single xdat:user
							for (NamingEnumeration ae = attrs.getAll(); ae
									.hasMore();) {
								Attribute attr = (Attribute) ae.next();

								logger.debug("getUsers:answer:row:attr:"
										+ attr.getID() + "=" + attr.get());
								for (Map.Entry<String, String> e : params
										.entrySet()) {
									if (e.getKey().equalsIgnoreCase(
											attr.getID())) {
										i.setProperty(e.getValue(), attr.get());
										break;
									}

								}
							}
						} catch (NamingException e) {
							logger.error(username
									+ ":getUser:Error retrieving data for "
									+ " from results", e);
						}
					}
					loginBean = new XDATUser(i);
					if (loginBean.getUsername() == null) {
						logger.error(username
								+ ":getUser:Missing login");
					} else {
						users.add(loginBean);
					}
				} catch (XFTInitException e) {
					logger.error("Problem during getUsers", e);
				} catch (ElementNotFoundException e) {
					logger.error("Problem during getUsers", e);
				} catch (FieldNotFoundException e) {
					logger.error("Problem during getUsers", e);
				} catch (Exception e) {
					logger.error("Problem during getUsers", e);
				}
			}

		} catch (NamingException e) {
			logger.error("Problem during getUsers", e);
		} finally {
			this.closeContext();
		}

		if (users.size() == 1) {
			logger.info(username + ":getUser:Matched user account:"
					+ users.get(0));
			return users.get(0);
		} else if (users.size() == 0) {
			logger.error(username + ":getUser:No Results Found");
			return null;
		} else {
			if (IGNORE_MULTIPLE_MATCHES) {
				logger.error(username
						+ ":getUser:Multiple matches ignored- returning first:"
						+ users.get(0));
				return users.get(0);
			} else {
				logger.error(username
						+ ":getUser:Multiple Records Found for "
						+ username + " Account");
				return null;
			}
		}
	}

	public String buildSearchFilter(Credentials cred) {
		return StringUtils.replace(SEARCH_TEMPLATE, "%USER%",
				cred.getUsername());
	}

	public String buildSearchFilter(String username) {
		return StringUtils.replace(SEARCH_TEMPLATE, "%USER%", username);
	}

	/**
	 * Query server for the DN for this cred
	 * 
	 * @param cred
	 * @return String DN
	 */
	public String getDN(Credentials cred) {
		logger.debug("\n\ngetDN:" + cred.getUsername());
		String searchFilter = buildSearchFilter(cred);
		return getDN(cred.getUsername(), searchFilter);
	}

	public String getDN(String username) {
		logger.debug("\n\ngetDN:" + username);
		String searchFilter = buildSearchFilter(username);
		return getDN(username, searchFilter);
	}

	public String getDN(String username, String searchFilter) {
		

		ArrayList<String> dns = new ArrayList<String>();
		// Create the search controls
		SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		String returnedAtts[] = { LDAP_USER_PK };
		searchCtls.setReturningAttributes(returnedAtts);
		try {
			this.openContext(LDAP_USER, LDAP_PASS);
			logger.debug(ctx.getEnvironment().toString());
			logger.debug("SEARCHBASE:" + SEARCHBASE);
			logger.debug("FILTER:" + searchFilter);
			logger.debug("searchCtls:" + searchCtls.toString());
			// Search for objects using the filter
			NamingEnumeration answer = ctx.search(SEARCHBASE, searchFilter,
					searchCtls);

			// Loop through the search results
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				String cn = sr.getClassName();

				Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					try {
						for (NamingEnumeration ae = attrs.getAll(); ae
								.hasMore();) {
							Attribute attr = (Attribute) ae.next();
							// System.out.println("Attribute: " + attr.getID());
							for (NamingEnumeration e = attr.getAll(); e
									.hasMore();) {
								if (attr.getID().equalsIgnoreCase(LDAP_USER_PK)) {
									dns.add((String) e.next());
								}
							}
						}
					} catch (NamingException e) {
						logger.error(username + ":Error retrieving DN for "
								+ " from results", e);
					}
				}
			}

		} catch (NamingException e) {
			logger.error(username + ":Error retrieving DN for " + username
					+ " from server", e);
		} finally {
			this.closeContext();
		}

		if (dns.size() == 1) {
			return dns.get(0);
		} else if (dns.size() == 0) {
			logger.info(username + ":No DN Found for " + username + " Account");
			return null;
		} else {
			logger.info(username + ":Multiple DNs Found for " + username
					+ " Account");
			return null;
		}
	}

	/**
	 * Attempt to login using the specified DN and password in cred.
	 * 
	 * @param dn
	 * @param cred
	 * @return
	 * @throws NamingException
	 *             (means authentication failure)
	 */
	public boolean attemptLogin(final String dn, final Credentials cred) {
		final String searchFilter = buildSearchFilter(cred);

		// Create the search controls
		final SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		final String returnedAtts[] = { LDAP_USER_PK };
		searchCtls.setReturningAttributes(returnedAtts);
		int count = 0;

		try {
			this.openContext(dn, cred.getPassword());
			logger.debug(ctx.getEnvironment().toString());
			logger.debug("SEARCHBASE:" + SEARCHBASE);
			logger.debug("FILTER:" + searchFilter);
			logger.debug("searchCtls:" + searchCtls.toString());
			// Search for objects using the filter
			NamingEnumeration answer = ctx.search(SEARCHBASE, searchFilter,
					searchCtls);

			// Loop through the search results
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				String cn = sr.getClassName();

				Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					try {
						for (NamingEnumeration ae = attrs.getAll(); ae
								.hasMore();) {
							Attribute attr = (Attribute) ae.next();
							// System.out.println("Attribute: " + attr.getID());
							for (NamingEnumeration e = attr.getAll(); e
									.hasMore();) {
								if (attr.getID().equalsIgnoreCase(LDAP_USER_PK)) {
									count++;
									e.next();
								}
							}
						}
					} catch (NamingException e) {
						logger.error(
								cred.getUsername()
										+ ":Error retrieving DN for "
										+ cred.getUsername() + " from results",
								e);
					}
				}
			}
		} catch (NamingException e) {
			logger.error(
					cred.getUsername() + ":Unable to authenticate "
							+ cred.getUsername()
							+ " with given password using DN: " + dn, e);
		} finally {
			this.closeContext();
		}

		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Step 1: Query server as user Step 2: Confirm results returned
	 * 
	 * @param cred
	 * @return
	 * @throws DataUseException
	 */
	public boolean verifyLogin(XDATUser u, Credentials cred)
			throws DataUseException {
		logger.debug("\n\nverifyLogin:" + cred.getUsername());
		String dn = (String) cred.OTHER.get(LDAP_USER_PK);
		if (dn == null) {
			logger.info(cred.getUsername() + ":failed to populate DN for "
					+ cred.getUsername() + " Account");
			return false;
		}

		if (this.attemptLogin(dn, cred)) {

			if (!acceptedDataUse(dn, cred)) {
				throw new DataUseException();
			}
			// Reflect changes made to the LDAP account to the local XNAT
			// account as necessary
			updateLocalAccount(u, cred);

			return true;
		} else {
			// check for updated DN
			final String newDN = this.getDN(cred);
			if (!dn.equals(newDN)) {
				logger.info(cred.getUsername()
						+ ":LDAP Server has a new DN for this user.  Attempting authentication with new DN "
						+ newDN);
				cred.OTHER.put(LDAP_USER_PK, dn);
				if (this.attemptLogin(newDN, cred)) {
					logger.info(cred.getUsername()
							+ ":LDAP authentication succeeded with updated DN "
							+ newDN + ". Updating stored DN.");
					updateStoredDN(newDN, u);
					return true;
				} else {
					return false;
				}
			} else {
				// don't update stored DN when authentication fails.
				return false;
			}
		}
	}

	public String retrieveStoredDN(XDATUser u) {
		// this version stores the DN in a dead field (quarantine_path). This
		// prevented having to modify the user schema, but really seems
		// in-appropriate.
		return u.getQuarantinePath();
	}

	public void updateStoredDN(final String newDN, XDATUser u) {
		try {
			u.setQuarantinePath(newDN);
			SaveItemHelper.authorizedSave(u, null, true, false, true, false,
					EventUtils.newEventInstance(EventUtils.CATEGORY.SIDE_ADMIN,
							EventUtils.TYPE.PROCESS, "Created user from LDAP"));
		} catch (Exception e) {
			logger.error(u.getUsername()
					+ ":Failed to update stored DN for user. Proceeding...", e);
		}
	}

	public boolean authenticate(XDATUser u, Credentials cred)
			throws PasswordAuthenticationException, EnabledException,
			ActivationException, Exception {
		if (!ENABLED)
			return super.authenticate(u, cred);
		try {
			if (u.getXdatUserId() != null) {
				if (!u.isEnabled()) {
					logger.info(u.getFirstname() + ": disabled");
					throw new XDATUser.EnabledException(u.getLogin());
				}

				if ((!u.isActive()) && (!u.checkRole("Administrator"))) {
					logger.info(u.getFirstname() + ": needs activation");
					throw new XDATUser.ActivationException(u.getLogin());
				}
			}

			if (StringUtils.isEmpty(u.getPrimaryPassword())) {
				AuthenticationAttempt attempt = HcpLdapAuthenticator
						.RetrieveCachedAttempt(cred);
				if (attempt == null) {
					if (!cred.OTHER.containsKey(LDAP_USER_PK)) {
						// DN is cached here to prevent an additional query.
						String dn = retrieveStoredDN(u);
						logger.debug(u.getUsername()
								+ ": CACHED quarantine path.");
						if (StringUtils.isEmpty(dn)) {
							// BUILD FIRST ATTEMPT, retrieve DN
							logger.info(u.getUsername()
									+ ": requesting DN from server.");
							dn = this.getDN(cred);
						}
						cred.OTHER.put(LDAP_USER_PK, dn);
					}

					attempt = new AuthenticationAttempt(cred);
					attempt.expire();
					RecordAttempt(attempt);
				} else if (!attempt.isExpired()) {
					if (attempt.cred.getPassword().equals(cred.getPassword())) {
						logger.info(u.getUsername()
								+ ": verified versus cache password. EXPIRES:"
								+ attempt.expires.getTime());
						return true;
					} else {
						attempt.expire();
						logger.info(u.getUsername()
								+ ": failed to verify against cached password. MIS-MATCH.");

					}
				}

				if (!attempt.cred.getPassword().equals(cred.getPassword())) {
					attempt.cred.setPassword(cred.getPassword());
				}

				// authenticate
				logger.info(u.getUsername()
						+ ": attempting to authenticate account from server.");
				if (this.verifyLogin(u, attempt.cred)) {
					logger.info(u.getUsername() + ": validated against server.");
					attempt.setTimeout();
					return true;
				} else {
					logger.info(u.getUsername()
							+ ": validation FAILED against server.");
					return false;
				}

			} else {
				try {
					boolean auth = super.authenticate(u, cred);
					if (auth) {
						logger.info(u.getUsername()
								+ ": validated against db value.");
						return true;
					} else {
						logger.info(u.getUsername()
								+ ": Wrong Password against db value.");
						return false;
					}
				} catch (PasswordAuthenticationException e) {
					logger.info(u.getUsername()
							+ ": Wrong Password against db value.");
					throw e;
				}
			}
		} catch (Exception t) {
			throw t;
		} finally {
			this.closeContext();
		}
	}

	private static Hashtable<String, AuthenticationAttempt> cache = new Hashtable<String, AuthenticationAttempt>();

	public synchronized static AuthenticationAttempt RetrieveCachedAttempt(
			Credentials cred) {
		if (cache.containsKey(cred.getUsername())) {
			AuthenticationAttempt attempt = cache.get(cred.getUsername());
			return attempt;
		} else {
			return null;
		}
	}

	public synchronized static void ClearCachedAttempt(Credentials cred) {
		ClearCachedAttempt(cred.getUsername());
	}

	private synchronized static void ClearCachedAttempt(String username) {
		if (cache.containsKey(username)) {
			cache.remove(username);
		}
	}

	public synchronized static void RecordAttempt(AuthenticationAttempt attempt) {
		if (!cache.containsKey(attempt.cred.getUsername())) {
			cache.put(attempt.cred.getUsername(), attempt);
		}
	}

	public static class AuthenticationAttempt {
		public Credentials cred = null;
		int attempts = 0;
		Calendar expires = null;

		public AuthenticationAttempt(Credentials c) {
			this.cred = c;
			setTimeout();
		}

		public void setTimeout() {
			expires = Calendar.getInstance();
			expires.add(Calendar.SECOND, AUTHENTICATION_EXPIRATION);
		}

		public int getAttempts() {
			return attempts;
		}

		public void expire() {
			expires = Calendar.getInstance();
		}

		public void setAttempts(int attempts) {
			this.attempts = attempts;
		}

		public boolean isExpired() {
			Calendar cp = Calendar.getInstance();
			if (cp.before(expires)) {
				return false;
			} else
				return true;
		}
	}

	public XDATUser authenticate(Credentials cred)
			throws PasswordAuthenticationException, EnabledException,
			ActivationException, Exception {
		if (!ENABLED)
			return super.authenticate(cred);
		try {
			XDATUser u = null;
			try {
				u = new XDATUser(cred.getUsername());
			} catch (UserNotFoundException e) {
				// ignore missing account for now. Need to check against LDAP
			}

			if (u == null && CREATE_MISSING_ACCOUNTS) {
				logger.info(cred.getUsername() + ": unknown user account");
				// SEE IF USER ACCOUNT exists in LDAP, and create
				u = getUsers(cred);

				if (u == null) {
					throw new XDATUser.FailedLoginException(
							"Unknown user account.", cred.getUsername());
				}

				u.setProperty("enabled", AUTO_ENABLE_ACCOUNTS);

				u.setProperty(
						"xdat:user.assigned_roles.assigned_role[0].role_name",
						"SiteUser");
				u.setProperty(
						"xdat:user.assigned_roles.assigned_role[1].role_name",
						"DataManager");
			}

			if (u != null) {
				if (authenticate(u, cred)) {
					if (u.getXdatUserId() == null) {

						SaveItemHelper.authorizedSave(u, null, true, false,
								true, false, EventUtils.newEventInstance(
										EventUtils.CATEGORY.SIDE_ADMIN,
										EventUtils.TYPE.PROCESS,
										"Updated user from LDAP"));

						u = new XDATUser(cred.getUsername());
						if (AUTO_ENABLE_ACCOUNTS) {
							u.setLoggedIn(true);
						} else {
							throw new EnabledException(u.getLogin());
						}
						if (!u.isExtended()) {
							u.init();
							if (!u.getItem().isPreLoaded())
								u.extend(true);
							u.setExtended(true);
						}
					} else {
						u.setLoggedIn(true);
					}
					return u;
				} else
					throw new PasswordAuthenticationException(u.getLogin());
			} else {
				throw new XDATUser.FailedLoginException("Unknown Account",
						cred.getUsername());
			}
		} catch (Exception t) {
			throw t;
		} finally {
			this.closeContext();
		}
	}

	public boolean isAutoEnabled() {
		return AUTO_ENABLE_ACCOUNTS;
	}

	public static String RegisterNewLdapAccount(String username, String email,
			String pw, String firstname, String lastname, String institution,
			String duAccepted, String wbAccepted) throws RegistrationException {
		final HcpLdapAuthenticator auth;
		try {
			auth = HcpLdapAuthenticator.class.newInstance();
			return auth.registerNewLdapAccount(username, email, pw, firstname,
					lastname, institution, duAccepted, wbAccepted);
		} catch (InstantiationException e) {
			throw new RegistrationException("Could not register LDAP account",
					e);
		} catch (IllegalAccessException e) {
			throw new RegistrationException("Could not register LDAP account",
					e);
		}
	}

	public String registerNewLdapAccount(String username, String email,
			String pw, String firstname, String lastname, String institution,
			String duAccepted, String wbAccepted) throws RegistrationException {
		try {
			final int UF_ACCOUNTDISABLE = 0x0002;
			final int UF_PASSWD_NOTREQD = 0x0020;
			final int UF_PASSWD_CANT_CHANGE = 0x0040;
			final int UF_NORMAL_ACCOUNT = 0x0200;
			final int UF_DONT_EXPIRE_PASSWD = 0x10000;
			final int UF_PASSWORD_EXPIRED = 0x800000;

			if (USER_CONTEXT == null || USER_CONTEXT.length() < 1) {
				throw new RegistrationException(
						"Could not register LDAP account - User context unknown");
			}

			// Make sure no existing XNAT account for user. Would result in
			// unusable account for LDAP user
			if (isXdatUser(email)) {
				throw new RegistrationException(
						"Could not register LDAP account - An account already exists for this Username");
			}

			final String dn = "CN=" + username + "," + USER_CONTEXT;

			this.openContext(LDAP_USER, LDAP_PASS);

			final BasicAttributes attrs = new BasicAttributes(true);
			final Attribute objclass = new BasicAttribute("objectClass");
			for (String clazz : USER_CLASS) {
				objclass.add(clazz);
			}
			attrs.put(objclass);

			final Attribute cnA = new BasicAttribute("cn");
			cnA.add(username);
			attrs.put(cnA);

			final Attribute samA = new BasicAttribute("sAMAccountName");
			samA.add(username);
			attrs.put(samA);

			final Attribute instA = new BasicAttribute("department");
			instA.add(institution);
			attrs.put(instA);

			final Attribute emailA = new BasicAttribute("mail");
			emailA.add(email);
			attrs.put(emailA);

			final Attribute uidA = new BasicAttribute("uid");
			uidA.add(username);
			attrs.put(uidA);

			final Attribute gnA = new BasicAttribute("givenName");
			gnA.add(firstname);
			attrs.put(gnA);

			final Attribute snA = new BasicAttribute("sn");
			snA.add(lastname);
			attrs.put(snA);

			if (duAccepted.equalsIgnoreCase("true")
					|| duAccepted.equalsIgnoreCase("y")
					|| duAccepted.equalsIgnoreCase("yes")) {
				final Attribute commentA = new BasicAttribute("comment");
				commentA.add(DATAUSE_ACCEPT + " ("
						+ new SimpleDateFormat("yyyy-MM-dd").format(new Date())
						+ ")");
				attrs.put(commentA);
			}
			if (wbAccepted.equalsIgnoreCase("true")
					|| wbAccepted.equalsIgnoreCase("y")
					|| wbAccepted.equalsIgnoreCase("yes")) {
				final Attribute commentA = new BasicAttribute("comment");
				commentA.add(WB_ACCEPT + " ("
						+ new SimpleDateFormat("yyyy-MM-dd").format(new Date())
						+ ")");
				attrs.put(commentA);
			}

			// Set account password
			try {
				attrs.put("unicodePwd", getActiveDirectoryEncodedPW(pw));
			} catch (UnsupportedEncodingException e) {
				throw new RegistrationException(
						"Registration couldn't be completed.  Could not set account password.  "
								+ "Verify that password complexity requirements have been met.",
						e);
			}

			// Enable account, set control settings
			attrs.put("userAccountControl",
					Integer.toString(UF_NORMAL_ACCOUNT + UF_DONT_EXPIRE_PASSWD));

			// Create user context
			ctx.createSubcontext(dn, attrs);

			// Set display name (fails when set above. Must be modified post
			// context account creation)
			try {
				ModificationItem mod[] = new ModificationItem[1];
				mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						new BasicAttribute("displayName", firstname + " "
								+ lastname));
				ctx.modifyAttributes(dn, mod);
			} catch (NamingException e) {
				logger.error("Registration couldn't be completed.  Could not set display name", e);
				throw new RegistrationException(
						"Registration couldn't be completed.  Could not set display name",
						e);
			}

			// Add user to groups
			try {
				// Modification MRH 2012-09-06. Now registering ConnectomeDB
				// access without consenting to any
				// data use terms. Consent will be done from inside the DB to
				// gain access to projects.
				ModificationItem mod[] = new ModificationItem[1];
				mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						new BasicAttribute("member", dn));
				for (String group : USER_GROUPS) {
					ctx.modifyAttributes(group, mod);
				}
				if (duAccepted.equalsIgnoreCase("true")
						|| duAccepted.equalsIgnoreCase("y")
						|| duAccepted.equalsIgnoreCase("yes")) {
					for (String group : FTP_GROUPS) {
						ctx.modifyAttributes(group, mod);
					}
				} else if (wbAccepted.equalsIgnoreCase("true")
						|| wbAccepted.equalsIgnoreCase("y")
						|| wbAccepted.equalsIgnoreCase("yes")) {
					for (String group : WB_GROUPS) {
						ctx.modifyAttributes(group, mod);
					}
				}
			} catch (NamingException e) {
				logger.error(
						"Registration couldn't be completed.  Could not set groups",
						e);
				throw new RegistrationException(
						"Registration couldn't be completed.  Could not set groups",
						e);
			}

			return dn;

		} catch (NamingException e) {
			if (e instanceof NameAlreadyBoundException) {
				logger.error(
						"Could not complete registration - An account already exists for this Username (" + username + ")",
						e);
				throw new RegistrationException(
						"Could not complete registration - An account already exists for this Username",
						e);
			}
			logger.error(
					"Could not complete registration",
					e);
			throw new RegistrationException("Could not complete registration",
					e);
		} finally {
			this.closeContext();
		}

	}

	private byte[] getActiveDirectoryEncodedPW(String inpw)
			throws UnsupportedEncodingException {
		String quotedPW = "\"" + inpw + "\"";
		return quotedPW.getBytes("UTF-16LE");
	}

	public boolean isXdatUser(String login) {

		try {

			SchemaElementI e = SchemaElement.GetElement(XDAT_USER_ELEMENT);
			ItemSearch search = new ItemSearch(null, e.getGenericXFTElement());
			search.addCriteria(
					XDAT_USER_ELEMENT + XFT.PATH_SEPERATOR + "login", login);
			search.setLevel(ViewManager.ACTIVE);
			ArrayList found = search.exec(true).items();

			if (found.size() > 0) {
				return true;
			}

			return false;

		} catch (Exception e) {
			// Assume no current xdat user for these purposes
			return false;
		}

	}

	public static boolean RegisterDataUseAcceptance(String username, String pw) {
		final HcpLdapAuthenticator auth;
		try {
			auth = HcpLdapAuthenticator.class.newInstance();
			return auth.registerDataUseAcceptance(username, pw);
		} catch (InstantiationException e) {
			// Do nothing for now
		} catch (IllegalAccessException e) {
			// Do nothing for now
		}
		return false;
	}

	private boolean registerDataUseAcceptance(String username, String pw) {

		final String searchFilter = StringUtils.replace(SEARCH_TEMPLATE,
				"%USER%", username);

		final String dn = "CN=" + username + "," + USER_CONTEXT;

		// Create the search controls
		final SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		final String returnedAtts[] = { "comment" };
		searchCtls.setReturningAttributes(returnedAtts);

		try {
			// First open context with user permissions, to ensure
			// authentication
			try {
				this.openContext(dn, pw);
				this.closeContext();
			} catch (AuthenticationException e) {
				return false;
			} catch (Exception e) {
				return false;
			}

			// Then open with RW account permissions
			this.openContext(LDAP_USER, LDAP_PASS);
			NamingEnumeration answer = ctx.search(SEARCHBASE, searchFilter,
					searchCtls);
			// Loop through the search results
			String currComment = "";
			if (!answer.hasMore()) {
				ModificationItem mod[] = new ModificationItem[1];
				mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						new BasicAttribute("comment", DATAUSE_ACCEPT
								+ " ("
								+ new SimpleDateFormat("yyyy-MM-dd")
										.format(new Date()) + ")"));
				ctx.modifyAttributes(dn, mod);
				try {
					mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							new BasicAttribute("member", dn));
					for (String group : FTP_GROUPS) {
						ctx.modifyAttributes(group, mod);
					}
				} catch (Exception ie) {
					// Do nothing for now
				}
				return true;
			}
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					for (NamingEnumeration ae = attrs.getAll(); ae
							.hasMore();) {
						Attribute attr = (Attribute) ae.next();
						// System.out.println("Attribute: " + attr.getID());
						if (attr.getID().equalsIgnoreCase("comment")) {
							for (NamingEnumeration e = attr.getAll(); e
									.hasMore();) {
								if (currComment == null
										|| currComment.length() < 1) {
									currComment = e.next().toString();
								} else {
									currComment = currComment + "; "
											+ e.next().toString();
								}
								if (currComment.indexOf(DATAUSE_ACCEPT) >= 0) {
									return true;
								}
							}
						}
					}
				}
			}
				ModificationItem mod[] = new ModificationItem[1];
				if (currComment == null || currComment.trim().length() < 1) {
					mod[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
							new BasicAttribute("comment", DATAUSE_ACCEPT
									+ " ("
									+ new SimpleDateFormat("yyyy-MM-dd")
											.format(new Date()) + ")"));
				} else {
					mod[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
							new BasicAttribute("comment", currComment
									+ "; "
									+ DATAUSE_ACCEPT
									+ " ("
									+ new SimpleDateFormat("yyyy-MM-dd")
											.format(new Date()) + ")"));

				}
				ctx.modifyAttributes(dn, mod);
				try {
					mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							new BasicAttribute("member", dn));
					for (String group : FTP_GROUPS) {
						ctx.modifyAttributes(group, mod);
					}
				} catch (Exception ie) {
					// Do nothing for now
				}
				return true;
		} catch (NamingException e) {
			logger.error(
					"Could not register data use acceptance",
					e);
			return false;
		} finally {
			this.closeContext();
		}

	}

	public static boolean RegisterWBDataUseAcceptance(String username, String pw) {
		final HcpLdapAuthenticator auth;
		try {
			auth = HcpLdapAuthenticator.class.newInstance();
			return auth.registerWBDataUseAcceptance(username, pw);
		} catch (InstantiationException e) {
			// Do nothing for now
		} catch (IllegalAccessException e) {
			// Do nothing for now
		}
		return false;
	}

	private boolean registerWBDataUseAcceptance(String username, String pw) {

		final String searchFilter = StringUtils.replace(SEARCH_TEMPLATE,
				"%USER%", username);

		final String dn = "CN=" + username + "," + USER_CONTEXT;

		// Create the search controls
		final SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		final String returnedAtts[] = { "comment" };
		searchCtls.setReturningAttributes(returnedAtts);

		try {
			// First open context with user permissions, to ensure
			// authentication
			try {
				this.openContext(dn, pw);
				this.closeContext();
			} catch (AuthenticationException e) {
				return false;
			} catch (Exception e) {
				return false;
			}

			// Then open with RW account permissions
			this.openContext(LDAP_USER, LDAP_PASS);
			NamingEnumeration answer = ctx.search(SEARCHBASE, searchFilter,
					searchCtls);
			// Loop through the search results
			String currComment = "";
			if (!answer.hasMore()) {
				ModificationItem mod[] = new ModificationItem[1];
				mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						new BasicAttribute("comment", WB_ACCEPT
								+ " ("
								+ new SimpleDateFormat("yyyy-MM-dd")
										.format(new Date()) + ")"));
				ctx.modifyAttributes(dn, mod);
				try {
					mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							new BasicAttribute("member", dn));
					for (String group : WB_GROUPS) {
						ctx.modifyAttributes(group, mod);
					}
				} catch (Exception ie) {
					// Do nothing for now
				}
				return true;
			}
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					for (NamingEnumeration ae = attrs.getAll(); ae
							.hasMore();) {
						Attribute attr = (Attribute) ae.next();
						// System.out.println("Attribute: " + attr.getID());
						if (attr.getID().equalsIgnoreCase("comment")) {
							for (NamingEnumeration e = attr.getAll(); e
									.hasMore();) {
								if (currComment == null
										|| currComment.length() < 1) {
									currComment = e.next().toString();
								} else {
									currComment = currComment + "; "
											+ e.next().toString();
								}
								if (currComment.indexOf(WB_ACCEPT) >= 0) {
									return true;
								}
							}
						}
					}
				}
			}
			ModificationItem mod[] = new ModificationItem[1];
			if (currComment == null || currComment.trim().length() < 1) {
				mod[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						new BasicAttribute("comment", WB_ACCEPT
								+ " ("
								+ new SimpleDateFormat("yyyy-MM-dd")
										.format(new Date()) + ")"));
			} else {
				mod[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						new BasicAttribute("comment", currComment
								+ "; "
								+ WB_ACCEPT
								+ " ("
								+ new SimpleDateFormat("yyyy-MM-dd")
										.format(new Date()) + ")"));

			}
			ctx.modifyAttributes(dn, mod);
			try {
				mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						new BasicAttribute("member", dn));
				for (String group : WB_GROUPS) {
					ctx.modifyAttributes(group, mod);
				}
			} catch (Exception ie) {
				// Do nothing for now
			}
			return true;
		} catch (NamingException e) {
			logger.error(e);
			return false;
		} finally {
			this.closeContext();
		}

	}
	
	/////////////////////////////////////
	// DATA USE TERMS RESOURCE SUPPORT //
	/////////////////////////////////////
	
	public static boolean RecordDataUseAcceptance(String username,String terms) throws DataUseException {
		final HcpLdapAuthenticator auth;
		try {
			auth = HcpLdapAuthenticator.class.newInstance();
			return auth.recordDataUseAcceptance(username,terms);
		} catch (InstantiationException e) {
			// Do nothing for now
		} catch (IllegalAccessException e) {
			// Do nothing for now
		}
		return false;
	}
	
	// This is mostly the method from the BaseXnatProjectdata class, however it allows a userid that doesn't normally have access to add
	// their self as a collaborator.  This seems preferable than acting as project owner (acquiring that username) and doing the add
    private void addGroupMember(XnatProjectdata proj, XDATUser newUser, EventMetaI ci, boolean checkExisting) throws Exception{
	        String group_id = proj.getId() + "_" + BaseXnatProjectdata.COLLABORATOR_GROUP;
	    	final String confirmquery = "SELECT * FROM xdat_user_groupid WHERE groupid='" + group_id + "' AND groups_groupid_xdat_user_xdat_user_id=" + newUser.getXdatUserId() + ";";
	    	boolean isOwner=false;
	    	if(checkExisting){
				for (Map.Entry<String, UserGroup> entry : newUser.getGroups().entrySet()) {
					if (entry.getValue().getTag()!=null && entry.getValue().getTag().equals(proj.getId())) {
						if(entry.getValue().getId().equals(group_id)){
						}

						//find mapping object to delete
						for (XdatUserGroupid map : newUser.getGroups_groupid()) {
							if (map.getGroupid().equals(entry.getValue().getId())) {
								if(!map.getGroupid().endsWith("_owner")){
									SaveItemHelper.authorizedDelete(map.getItem(), newUser,ci);
								}else{
									throw new ClientException(Status.CLIENT_ERROR_CONFLICT,"User is already an owner of this project.",new Exception());
								}
							}
						}
					}
				}
	    	}

	    	if(!isOwner){
				XFTTable t=XFTTable.Execute(confirmquery,newUser.getDBName(), newUser.getUsername());
		    	if(t.size()==0){
		            final XdatUserGroupid map = new XdatUserGroupid((UserI)newUser);
		            map.setProperty(map.getXSIType() +".groups_groupid_xdat_user_xdat_user_id", newUser.getXdatUserId());
		            map.setGroupid(group_id);
		            SaveItemHelper.authorizedSave(map,newUser, false, false,ci);
		    	}
	    	}
    }
    

	// This is mostly the method from the BaseXnatProjectdata class, however it allows a userid that doesn't normally have access to remove
	// their self as a collaborator.  This seems preferable than acting as project owner (acquiring that username) and doing the remove
    public void removeGroupMember(XnatProjectdata proj, XDATUser newUser,EventDetails ci) throws Exception{
	        String group_id = proj.getId() + "_" + BaseXnatProjectdata.COLLABORATOR_GROUP;
    	final String confirmquery = "SELECT * FROM xdat_user_groupid WHERE groupid='" + group_id + "' AND groups_groupid_xdat_user_xdat_user_id=" + newUser.getXdatUserId() + ";";
    	XFTTable t=XFTTable.Execute(confirmquery,newUser.getDBName(), newUser.getUsername());
    	if(t.size()>0){
    		final String query = "DELETE FROM xdat_user_groupid WHERE groupid='" + group_id + "' AND groups_groupid_xdat_user_xdat_user_id=" + newUser.getXdatUserId() + ";";

    		PersistentWorkflowI wrk= PersistentWorkflowUtils.buildOpenWorkflow(newUser, newUser.getXSIType(), newUser.getXdatUserId().toString(), proj.getId(), ci);
    		try {
				PoolDBUtils.ExecuteNonSelectQuery(query,proj.getItem().getDBName(), newUser.getLogin());

				PoolDBUtils.PerformUpdateTrigger(newUser.getItem(), newUser.getLogin());
				PersistentWorkflowUtils.complete(wrk, wrk.buildEvent());
			} catch (Exception e) {
				PersistentWorkflowUtils.fail(wrk,wrk.buildEvent());
				throw e;
			}
    	}
    }
    
	private boolean recordDataUseAcceptance(String username,String terms) throws DataUseException {
		
		String acceptString = null;
		String[] groupsArray = {};
		String[] projectsArray = {};
		XDATUser user = getUsers(username);
		
		if (terms.equalsIgnoreCase("WB")) {
			
			acceptString = WB_ACCEPT;
			groupsArray = WB_GROUPS;
			projectsArray = WB_PROJECTS;
			
		} else if (terms.equalsIgnoreCase("PHASE1")) {
			
			acceptString = PHASE1_ACCEPT;
			groupsArray = PHASE1_GROUPS;
			projectsArray = PHASE1_PROJECTS;
			
		} else if (terms.equalsIgnoreCase("PHASE2")) {
			
			acceptString = PHASE2_ACCEPT;
			groupsArray = PHASE2_GROUPS;
			projectsArray = PHASE2_PROJECTS;
			
		} else if (terms.equalsIgnoreCase("NKI")) {
			
			acceptString = NKI_ACCEPT;
			groupsArray = NKI_GROUPS;
			projectsArray = NKI_PROJECTS;
			
		} else {
			throw new DataUseException("Invalid value for terms (" + terms + ")");
		}
		
		// Add user to projects specified in configuration file.
		for (String projS : projectsArray) {
			XDATUser newUser = null;
			XnatProjectdata proj = XnatProjectdata.getXnatProjectdatasById(projS, newUser, false);
			try {
				// Create newUser object because user from getUsers method doesn't have xnat ID assigned
				newUser = new XDATUser(user.getLogin());
				
				final PersistentWorkflowI wrk=PersistentWorkflowUtils.buildOpenWorkflow(newUser, proj.getItem(),
						EventUtils.newEventInstance(EventUtils.CATEGORY.PROJECT_ACCESS,EventUtils.TYPE.PROCESS, EventUtils.ADD_USER_TO_PROJECT));
				EventMetaI c = wrk.buildEvent();
	            addGroupMember(proj, newUser, WorkflowUtils.setStep(wrk, "Add " + newUser.getLogin()),true);
		        PersistentWorkflowUtils.complete(wrk, c);
			
			} catch (Exception e) {
				throw new DataUseException("ERROR:  Could not add users to project " + proj.getId(),e);
			}
	    }
		
		final String searchFilter = StringUtils.replace(SEARCH_TEMPLATE,
				"%USER%", username);

		final String dn = "CN=" + username + "," + USER_CONTEXT;
		
		/////////////////////////////
		// MODIFY GROUP MEMBERSHIP //
		/////////////////////////////
		
		try {
		
			final SearchControls searchCtls = new SearchControls();
			String returnedAtts2[] = { "memberOf" };
			searchCtls.setReturningAttributes(returnedAtts2);
			boolean alreadyMember = false;
	
				this.openContext(LDAP_USER, LDAP_PASS);
				logger.debug(ctx.getEnvironment().toString());
				logger.debug("SEARCHBASE:" + SEARCHBASE);
				logger.debug("FILTER:" + searchFilter);
				logger.debug("searchCtls:" + searchCtls.toString());
				// Search for objects using the filter
				NamingEnumeration answer = ctx.search(SEARCHBASE, searchFilter,
						searchCtls);
	
				// Loop through the search results
				while (answer.hasMoreElements()) {
					SearchResult sr = (SearchResult) answer.next();
					String cn = sr.getClassName();
	
					Attributes attrs = sr.getAttributes();
					if (attrs != null) {
						try {
							for (NamingEnumeration ae = attrs.getAll(); ae
									.hasMore();) {
								Attribute attr = (Attribute) ae.next();
								// System.out.println("Attribute: " + attr.getID());
								if (attr.getID().equalsIgnoreCase("memberOf")) {
									for (NamingEnumeration e = attr.getAll(); e
											.hasMore();) {
										String member = e.next().toString();
										for (String group : groupsArray) {
											if (member.equalsIgnoreCase(group)) {
												alreadyMember = true;
											}
										}
									}
								}
							}
						} catch (NamingException e) {
							logger.error(
									LDAP_USER
											+ ":Error retrieving DN for "
											+ LDAP_USER + " from results",
									e);
						}
					}
				}
					
				if (!alreadyMember) {
					try {
						ModificationItem mod[] = new ModificationItem[1];
						mod = new ModificationItem[1];
						mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
								new BasicAttribute("member", dn));
						for (String group : groupsArray) {
							ctx.modifyAttributes(group, mod);
						}
					} catch (Exception ie) {
						// Do nothing for now
					}
				}
			
			////////////////////
			// MODIFY COMMENT //
			////////////////////
	
			// Create the search controls
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String returnedAtts[] = { "comment" };
			searchCtls.setReturningAttributes(returnedAtts);
	
			// Then open with RW account permissions
			this.openContext(LDAP_USER, LDAP_PASS);
			answer = ctx.search(SEARCHBASE, searchFilter,
					searchCtls);
			// Loop through the search results
			String currComment = "";
			if (!answer.hasMore()) {
					ModificationItem mod[] = new ModificationItem[1];
					mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							new BasicAttribute("comment", acceptString
									+ " ("
									+ new SimpleDateFormat("yyyy-MM-dd")
											.format(new Date()) + ")"));
					ctx.modifyAttributes(dn, mod);
					try {
						mod[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
								new BasicAttribute("member", dn));
						for (String group : groupsArray) {
							ctx.modifyAttributes(group, mod);
						}
					} catch (Exception ie) {
						// Do nothing for now
					}
			}
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				Attributes attrs = sr.getAttributes();
				if (attrs != null) {
						for (NamingEnumeration ae = attrs.getAll(); ae
								.hasMore();) {
							Attribute attr = (Attribute) ae.next();
							// System.out.println("Attribute: " + attr.getID());
							if (attr.getID().equalsIgnoreCase("comment")) {
								for (NamingEnumeration e = attr.getAll(); e
										.hasMore();) {
									if (currComment == null
											|| currComment.length() < 1) {
										currComment = e.next().toString();
									} else {
										currComment = currComment + "; "
												+ e.next().toString();
									}
								}
							}
						}
				}
			}
			currComment = currComment.replaceAll("[; ]*" + acceptString + "[^)]*[)]", "");
			if (currComment.startsWith("; ")) {
				currComment = currComment.substring(2);
			}
			ModificationItem mod[] = new ModificationItem[1];
			if (currComment == null || currComment.trim().length() < 1) {
				mod[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						new BasicAttribute("comment", acceptString
								+ " ("
								+ new SimpleDateFormat("yyyy-MM-dd")
										.format(new Date()) + ")"));
			} else {
				mod[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						new BasicAttribute("comment", currComment
								+ "; "
								+ acceptString
								+ " ("
								+ new SimpleDateFormat("yyyy-MM-dd")
										.format(new Date()) + ")"));
	
			}
			ctx.modifyAttributes(dn, mod);
			return true;
			
		} catch (NamingException e) {
			logger.error(e);
			return false;
		} finally {
			this.closeContext();
		}

	}
	
	public static boolean RemoveDataUseAcceptance(String username,String terms) throws DataUseException {
		final HcpLdapAuthenticator auth;
		try {
			auth = HcpLdapAuthenticator.class.newInstance();
			return auth.removeDataUseAcceptance(username,terms);
		} catch (InstantiationException e) {
			// Do nothing for now
		} catch (IllegalAccessException e) {
			// Do nothing for now
		}
		return false;
	}

	private boolean removeDataUseAcceptance(String username,String terms) throws DataUseException {
		
		XDATUser user = getUsers(username);
		String acceptString = null;
		String[] groupsArray = {};
		String[] projectsArray = {};
		if (terms.equalsIgnoreCase("WB")) {
			acceptString = WB_ACCEPT;
			groupsArray = WB_GROUPS;
			projectsArray = WB_PROJECTS;
		} else if (terms.equalsIgnoreCase("PHASE1")) {
			acceptString = PHASE1_ACCEPT;
			groupsArray = PHASE1_GROUPS;
			groupsArray = PHASE1_PROJECTS;
		} else if (terms.equalsIgnoreCase("PHASE2")) {
			acceptString = PHASE2_ACCEPT;
			groupsArray = PHASE2_GROUPS;
			groupsArray = PHASE2_PROJECTS;
		} else if (terms.equalsIgnoreCase("NKI")) {
			acceptString = NKI_ACCEPT;
			groupsArray = NKI_GROUPS;
			groupsArray = NKI_PROJECTS;
		} else {
			throw new DataUseException("Invalid value for terms (" + terms + ")");
		}

		final String searchFilter = StringUtils.replace(SEARCH_TEMPLATE,
				"%USER%", username);

		final String dn = "CN=" + username + "," + USER_CONTEXT;
		
		// Remove users project access
		for (String projS : projectsArray) {
			XDATUser newUser = null;
			XnatProjectdata proj = XnatProjectdata.getXnatProjectdatasById(projS, newUser, false);
			try {
				// Create newUser object because user from getUsers method doesn't have xnat ID assigned
				newUser = new XDATUser(user.getLogin());
				
				EventDetails edet = EventUtils.newEventInstance(EventUtils.CATEGORY.PROJECT_ACCESS,EventUtils.TYPE.PROCESS, EventUtils.REMOVE_USER_TO_PROJECT);
				final PersistentWorkflowI wrk=PersistentWorkflowUtils.buildOpenWorkflow(newUser, proj.getItem(), edet);
				EventMetaI c = wrk.buildEvent();
	            removeGroupMember(proj, newUser, edet);
		        PersistentWorkflowUtils.complete(wrk, c);
			
			} catch (Exception e) {
				logger.error("ERROR:  Could not remove users from project " + proj.getId(),e);
			}
	    }

		// Create the search controls
		final SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		final String returnedAtts[] = { "comment" };
		searchCtls.setReturningAttributes(returnedAtts);

		try {
			// Then open with RW account permissions
			this.openContext(LDAP_USER, LDAP_PASS);
			NamingEnumeration answer = ctx.search(SEARCHBASE, searchFilter,
					searchCtls);
			// Loop through the search results
			String currComment = "";
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					try {
						for (NamingEnumeration ae = attrs.getAll(); ae
								.hasMore();) {
							Attribute attr = (Attribute) ae.next();
							// System.out.println("Attribute: " + attr.getID());
							if (attr.getID().equalsIgnoreCase("comment")) {
								for (NamingEnumeration e = attr.getAll(); e
										.hasMore();) {
									if (currComment == null
											|| currComment.length() < 1) {
										currComment = e.next().toString();
									} else {
										currComment = currComment + "; "
												+ e.next().toString();
									}
								}
							}
						}
					} catch (NamingException e) {
						return false;
					}
				}
			}
			try {
				ModificationItem mod[] = new ModificationItem[1];
				currComment = currComment.replaceAll("[; ]*" + acceptString + "[^)]*[)]", "");
				if (currComment.length()<1) {
					mod[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE,
							new BasicAttribute("comment"));
							//new BasicAttribute("comment", dn));
				} else {
					mod[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
							new BasicAttribute("comment", currComment));
				}
				ctx.modifyAttributes(dn, mod);
				try {
					mod[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE,
							new BasicAttribute("member", dn));
					for (String group : groupsArray) {
						ctx.modifyAttributes(group, mod);
					}
				} catch (Exception ie) {
					// Do nothing for now
				}
				return true;
			} catch (NamingException e) {
				return false;
			}
		} catch (NamingException e) {
			return false;
		} finally {
			this.closeContext();
		}

	}

	public static String GroupsByUsername(String user, String group) {
		final HcpLdapAuthenticator auth;
		try {
			auth = HcpLdapAuthenticator.class.newInstance();
			return auth.groupsByUsername(user, group);
		} catch (InstantiationException e) {
			// Do nothing for now
		} catch (IllegalAccessException e) {
			// Do nothing for now
		}
		return "";
	}
	
	public static String GroupsByUsername(String user) throws DataUseException {
		return GroupsByUsername(user,null);
	}
	
	private String groupsByUsername(String user, String group) {
		
		//private static String SEARCH_TEMPLATE = "(&(objectClass=user)(CN=%USER%))";
		
		final String searchFilter = StringUtils.replace(SEARCH_TEMPLATE,
				"%USER%", user);

		// Create the search controls
		return getGroups(searchFilter, group);
		
	}
	
	public static String GroupsByEmail(String mail, String group) throws DataUseException {
		final HcpLdapAuthenticator auth;
		try {
			auth = HcpLdapAuthenticator.class.newInstance();
			return auth.groupsByEmail(mail, group);
		} catch (InstantiationException e) {
			// Do nothing for now
		} catch (IllegalAccessException e) {
			// Do nothing for now
		}
		return "";
	}
	
	public static String GroupsByEmail(String mail) throws DataUseException {
		return GroupsByEmail(mail,null);
	}
	
	private String groupsByEmail(String mail, String group) {
		
		//private static String SEARCH_TEMPLATE = "(&(objectClass=user)(CN=%USER%))";
		
		final String searchFilter = StringUtils.replace(SEARCH_TEMPLATE,
				"CN=%USER%", "mail=" + mail);

		// Create the search controls
		return getGroups(searchFilter, group);
		
	}

	private String getGroups(String searchFilter, String group) {
		
		final SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		final String returnedAtts[] = { "memberOf" };
		searchCtls.setReturningAttributes(returnedAtts);
		StringBuilder rtn = new StringBuilder();

		try {
			this.openContext(LDAP_USER, LDAP_PASS);
			logger.debug(ctx.getEnvironment().toString());
			logger.debug("SEARCHBASE:" + SEARCHBASE);
			logger.debug("FILTER:" + searchFilter);
			logger.debug("searchCtls:" + searchCtls.toString());
			// Search for objects using the filter
			NamingEnumeration answer = ctx.search(SEARCHBASE, searchFilter,
					searchCtls);
			
			if (!answer.hasMoreElements()) {
				// Will return not found
				return null;
			}

			// Loop through the search results
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				String cn = sr.getClassName();

				Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					try {
						for (NamingEnumeration ae = attrs.getAll(); ae
								.hasMore();) {
							Attribute attr = (Attribute) ae.next();
							// System.out.println("Attribute: " + attr.getID());
							if (attr.getID().equalsIgnoreCase("memberOf")) {
								for (NamingEnumeration e = attr.getAll(); e
										.hasMore();) {
									String member = e.next().toString();
									if (rtn.length()<=0) {
										rtn.append(member);
									} else {
										rtn.append(",").append(member);
									}
								}
							}
						}
					} catch (NamingException e) {
						return null;
					}
				}
			}
		} catch (NamingException e) {
			return null;
		} finally {
			this.closeContext();
		}
		
		String[] rtnArr = rtn.toString().split(",");
		Iterator<String> it = Iterators.forArray(rtnArr);
		rtn.delete(0,rtn.length());
		while (it.hasNext()) {
			String part = it.next();
			if (part.startsWith("CN=")) {
				if (rtn.length()>0) {
					rtn.append(',');
				}
				if (group != null && part!=null) {
					if (group.equals(part.substring(3))) {
						return "true";
					}
				}
				rtn.append(part.substring(3));
			}
		}
		if (group != null) {
			return "false";
		}
		return rtn.toString();
		
	}

	public static boolean AcceptedDataUse(String username,String terms) throws DataUseException {
		final HcpLdapAuthenticator auth;
		try {
			auth = HcpLdapAuthenticator.class.newInstance();
			return auth.acceptedDataUse(username,terms);
		} catch (InstantiationException e) {
			// Do nothing for now
		} catch (IllegalAccessException e) {
			// Do nothing for now
		}
		return false;
	}

	private boolean acceptedDataUse(String username,String terms) throws DataUseException {
		
		String acceptString = null;
		String[] groupsArray = {};
		if (terms!=null && terms.equalsIgnoreCase("WB")) {
			acceptString = WB_ACCEPT;
			groupsArray = WB_GROUPS;
		} else if (terms!=null && terms.equalsIgnoreCase("PHASE1")) {
			acceptString = PHASE1_ACCEPT;
			groupsArray = PHASE1_GROUPS;
		} else if (terms!=null && terms.equalsIgnoreCase("PHASE2")) {
			acceptString = PHASE2_ACCEPT;
			groupsArray = PHASE2_GROUPS;
		} else if (terms!=null && terms.equalsIgnoreCase("NKI")) {
			acceptString = NKI_ACCEPT;
			groupsArray = NKI_GROUPS;
		} else {
			throw new DataUseException("Invalid value for terms (" + terms + ")");
		}

		final String searchFilter = StringUtils.replace(SEARCH_TEMPLATE,
				"%USER%", username);

		final String dn = "CN=" + username + "," + USER_CONTEXT;

		// Create the search controls
		final SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		final String returnedAtts[] = { "memberOf" };
		searchCtls.setReturningAttributes(returnedAtts);
		boolean acceptedDataUse = false;

		try {
			this.openContext(LDAP_USER, LDAP_PASS);
			logger.debug(ctx.getEnvironment().toString());
			logger.debug("SEARCHBASE:" + SEARCHBASE);
			logger.debug("FILTER:" + searchFilter);
			logger.debug("searchCtls:" + searchCtls.toString());
			// Search for objects using the filter
			NamingEnumeration answer = ctx.search(SEARCHBASE, searchFilter,
					searchCtls);

			// Loop through the search results
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				String cn = sr.getClassName();

				Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					try {
						for (NamingEnumeration ae = attrs.getAll(); ae
								.hasMore();) {
							Attribute attr = (Attribute) ae.next();
							// System.out.println("Attribute: " + attr.getID());
							if (attr.getID().equalsIgnoreCase("memberOf")) {
								for (NamingEnumeration e = attr.getAll(); e
										.hasMore();) {
									String member = e.next().toString();
									for (String group : groupsArray) {
										if (member.equalsIgnoreCase(group)) {
											acceptedDataUse = true;
										}
									}
								}
							}
						}
					} catch (NamingException e) {
						logger.error(
								LDAP_USER
										+ ":Error retrieving DN for "
										+ LDAP_USER + " from results",
								e);
					}
				}
			}
		} catch (NamingException e) {
			logger.error(
					LDAP_USER + ":Unable to authenticate "
							+ LDAP_USER
							+ " with given password using DN: " + dn, e);
		} finally {
			this.closeContext();
		}

		if (acceptedDataUse) {
			return true;
		} else {
			return false;
		}

	}
	
	/////////////////////////////////////////
	// END DATA USE TERMS RESOURCE SUPPORT //
	/////////////////////////////////////////
	
	private void updateLocalAccount(XDATUser u, Credentials cred) {

		// Reflect changes made to the LDAP account to the local XNAT account as
		// necessary

		final String searchFilter = StringUtils.replace(SEARCH_TEMPLATE,
				"%USER%", u.getUsername());

		final String dn = "CN=" + u.getUsername() + "," + USER_CONTEXT;

		// XdatUser user = XDATUser.getXdatUsersByLogin(username,
		// TurbineUtils.getUser(data), false);

		// Create the search controls
		final SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		final String returnedAtts[] = { "givenName", "sn", "mail" };
		searchCtls.setReturningAttributes(returnedAtts);

		try {
			// Then open with RW account permissions
			this.openContext(LDAP_USER, LDAP_PASS);
			NamingEnumeration answer = ctx.search(SEARCHBASE, searchFilter,
					searchCtls);
			// Loop through the search results
			String ldap_firstname = "";
			String ldap_lastname = "";
			String ldap_email = "";
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					try {
						for (NamingEnumeration ae = attrs.getAll(); ae
								.hasMore();) {
							Attribute attr = (Attribute) ae.next();
							// System.out.println("Attribute: " + attr.getID());
							if (attr.getID().equalsIgnoreCase("givenName")) {
								for (NamingEnumeration e = attr.getAll(); e
										.hasMore();) {
									if (ldap_firstname == null
											|| ldap_firstname.length() < 1) {
										ldap_firstname = e.next().toString();
									} else {
										ldap_firstname = ldap_firstname + " "
												+ e.next().toString();
									}
								}
							} else if (attr.getID().equalsIgnoreCase("sn")) {
								for (NamingEnumeration e = attr.getAll(); e
										.hasMore();) {
									if (ldap_lastname == null
											|| ldap_lastname.length() < 1) {
										ldap_lastname = e.next().toString();
									} else {
										ldap_lastname = ldap_lastname + " "
												+ e.next().toString();
									}
								}
							} else if (attr.getID().equalsIgnoreCase("mail")) {
								for (NamingEnumeration e = attr.getAll(); e
										.hasMore();) {
									if (ldap_email == null
											|| ldap_email.length() < 1) {
										ldap_email = e.next().toString();
									} else {
										ldap_email = ldap_email + " "
												+ e.next().toString();
									}
								}
							}
						}
					} catch (NamingException e) {
						// Do nothing for now
					}
				}
			}
			boolean isModified = false;
			if (!ldap_firstname.equals(u.getFirstname())) {
				u.setFirstname(ldap_firstname);
				isModified = true;
			}
			if (!ldap_lastname.equals(u.getLastname())) {
				u.setLastname(ldap_lastname);
				isModified = true;
			}
			if (!ldap_email.equals(u.getEmail())) {
				u.setEmail(ldap_email);
				isModified = true;
			}
			if (isModified) {
				try {
					SaveItemHelper.authorizedSave(u, null, true, false, true,
							false, EventUtils.newEventInstance(
									EventUtils.CATEGORY.SIDE_ADMIN,
									EventUtils.TYPE.PROCESS,
									"Updated user from LDAP"));
				} catch (Exception e) {
					// Do nothing for now
					e.printStackTrace();
				}
			}
		} catch (NamingException e) {
			// Do nothing for now
		} finally {
			this.closeContext();
		}
	}

	public static void UpdateLdapAccount(RunData data,
			Hashtable<String, String> props) throws AccountUpdateException {
		final HcpLdapAuthenticator auth;
		try {
			auth = HcpLdapAuthenticator.class.newInstance();
			if (props.containsKey("login")) {
				auth.updateLdapAccount(XDATUser.getXdatUsersByLogin(
						props.get("login"), TurbineUtils.getUser(data), false),
						props);
			}
		} catch (InstantiationException e) {
			throw new AccountUpdateException("Could not update LDAP account", e);
		} catch (IllegalAccessException e) {
			throw new AccountUpdateException("Could not update LDAP account", e);
		}

	};

	private void updateLdapAccount(XdatUser user,
			Hashtable<String, String> props) throws AccountUpdateException {
		try {
			Set<Map.Entry<String, String>> entryset = props.entrySet();

			String firstname = null;
			String lastname = null;
			for (Map.Entry<String, String> entry : entryset) {

				if (entry.getKey().equals("primary_password")) {

					modifyLdapAttribute(getDN(user.getLogin()), "unicodePwd",
							getActiveDirectoryEncodedPW(entry.getValue()));
					ClearCachedAttempt(user.getLogin());

				} else if (entry.getKey().equals("email")) {

					modifyLdapAttribute(user, "mail", entry.getValue());

				} else if (entry.getKey().equals("firstname")) {

					firstname = entry.getValue();
					modifyLdapAttribute(user, "givenName", entry.getValue());

				} else if (entry.getKey().equals("lastname")) {

					lastname = entry.getValue();
					modifyLdapAttribute(user, "sn", entry.getValue());

				}

			}
			if (firstname != null && lastname != null) {
				modifyLdapAttribute(user, "displayName", firstname + " "
						+ lastname);
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("Could not update LDAP account", e);
			throw new AccountUpdateException("Could not update LDAP account", e);
		}

	}

	public static void UpdateLdapAccountPW(RunData data, String newPW)
			throws AccountUpdateException {
		final HcpLdapAuthenticator auth;
		try {
			ClearCachedAttempt(TurbineUtils.getUser(data).getLogin());
			auth = HcpLdapAuthenticator.class.newInstance();
			auth.updateLdapAccountPW(TurbineUtils.getUser(data), newPW);
		} catch (InstantiationException e) {
			logger.error("Could not update LDAP account", e);
			throw new AccountUpdateException("Could not update LDAP account", e);
		} catch (IllegalAccessException e) {
			logger.error("Could not update LDAP account", e);
			throw new AccountUpdateException("Could not update LDAP account", e);
		}
	}

	private void updateLdapAccountPW(XDATUser user, String newPW)
			throws AccountUpdateException {
		//// These checks don't work as of XNAT 1.6
		//if (!(user.isEnabled() && user.isLoggedIn())) {
		//	throw new AccountUpdateException(
		//			"Couldn't update LDAP account.  User not logged in.");
		//}
		try {
			modifyLdapAttribute(getDN(user.getLogin()), "unicodePwd",
					getActiveDirectoryEncodedPW(newPW));
		} catch (UnsupportedEncodingException e) {
			logger.error("Could not update LDAP account", e);
			throw new AccountUpdateException(
					"Couldn't update LDAP account.  Could not modify attribute.");
		}
	}

	public static String AccountPasswordReset(String username)
			throws AccountUpdateException {
		final HcpLdapAuthenticator auth;
		try {
			auth = HcpLdapAuthenticator.class.newInstance();
			return auth.accountPasswordReset(username);
		} catch (InstantiationException e) {
			logger.error("Could not update LDAP account", e);
			throw new AccountUpdateException("Could not update LDAP account", e);
		} catch (IllegalAccessException e) {
			logger.error("Could not update LDAP account", e);
			throw new AccountUpdateException("Could not update LDAP account", e);
		}
	}

	private String accountPasswordReset(String username)
			throws AccountUpdateException {
		try {
			// LDAP requires mixed-case passwords, XFT function returns only
			// lower-case character values.
			String newPW = Character.toUpperCase(XFT
					.CreateRandomCharacter(new Random()))
					+ XFT.CreateRandomAlphaNumeric(9);
			modifyLdapAttribute(getDN(username), "unicodePwd",
					getActiveDirectoryEncodedPW(newPW));
			return newPW;
		} catch (UnsupportedEncodingException e) {
			logger.error("Could not update LDAP account", e);
			throw new AccountUpdateException("Could not update LDAP account", e);
		} catch (AccountUpdateException e) {
			logger.error("Could not update LDAP account", e);
			throw new AccountUpdateException(
					"Could not update LDAP account.  Verify complexity requirements met.",
					e);
		}
	}

	public static void UpdateLdapAccountEmail(RunData data, String newEmail)
			throws AccountUpdateException {
		final HcpLdapAuthenticator auth;
		try {
			auth = HcpLdapAuthenticator.class.newInstance();
			auth.updateLdapAccountEmail(TurbineUtils.getUser(data), newEmail);
		} catch (InstantiationException e) {
			logger.error("Could not update LDAP account", e);
			throw new AccountUpdateException("Could not update LDAP account", e);
		} catch (IllegalAccessException e) {
			logger.error("Could not update LDAP account", e);
			throw new AccountUpdateException("Could not update LDAP account", e);
		}
	}

	private void updateLdapAccountEmail(XDATUser user, String newEmail)
			throws AccountUpdateException {
		// These checks don't work as of XNAT 1.6
		//if (!(user.isEnabled() && user.isLoggedIn())) {
		//	throw new AccountUpdateException(
		//			"Couldn't update LDAP account.  User not logged in.");
		//}
		modifyLdapAttribute(user, "mail", newEmail);
	}

	private void modifyLdapAttribute(XdatUser user, String attr, Object value)
			throws AccountUpdateException {
		
		String authUser = ((XDATUserDetails) user).getAuthorization().getAuthUser();
		String dn = getDN(authUser);
		modifyLdapAttribute(dn, attr, value);
		
	}

	private void modifyLdapAttribute(String dn, String attr, Object value)
			throws AccountUpdateException {
		try {
			this.openContext(LDAP_USER, LDAP_PASS);
			// Search for objects using the filter
			try {
				ModificationItem mod[] = new ModificationItem[1];
				mod[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						new BasicAttribute(attr, value));
				ctx.modifyAttributes(dn, mod);
			} catch (NamingException e) {
				logger.error("Couldn't update LDAP account.  Could not access modify attribute.");
				throw new AccountUpdateException(
						"Couldn't update LDAP account.  Could not modify attribute.");
			}

		} catch (NamingException e) {
			logger.error("Couldn't update LDAP account.  Could not access LDAP account.");
			throw new AccountUpdateException(
					"Couldn't update LDAP account.  Could not access LDAP account.");
		} finally {
			this.closeContext();
		}
	}

	private boolean acceptedDataUse(String dn, Credentials cred) {

		final String searchFilter = buildSearchFilter(cred);

		// Create the search controls
		final SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		final String returnedAtts[] = { "comment" };
		searchCtls.setReturningAttributes(returnedAtts);
		boolean acceptedDataUse = false;

		try {
			this.openContext(dn, cred.getPassword());
			logger.debug(ctx.getEnvironment().toString());
			logger.debug("SEARCHBASE:" + SEARCHBASE);
			logger.debug("FILTER:" + searchFilter);
			logger.debug("searchCtls:" + searchCtls.toString());
			// Search for objects using the filter
			NamingEnumeration answer = ctx.search(SEARCHBASE, searchFilter,
					searchCtls);

			// Loop through the search results
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				String cn = sr.getClassName();

				Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					try {
						for (NamingEnumeration ae = attrs.getAll(); ae
								.hasMore();) {
							Attribute attr = (Attribute) ae.next();
							// System.out.println("Attribute: " + attr.getID());
							if (attr.getID().equalsIgnoreCase("comment")) {
								for (NamingEnumeration e = attr.getAll(); e
										.hasMore();) {
									String comment = e.next().toString();
									if (comment.indexOf(DATAUSE_ACCEPT) >= 0) {
										acceptedDataUse = true;
									}
								}
							}
						}
					} catch (NamingException e) {
						logger.error(
								cred.getUsername()
										+ ":Error retrieving DN for "
										+ cred.getUsername() + " from results",
								e);
					}
				}
			}
		} catch (NamingException e) {
			logger.error(
					cred.getUsername() + ":Unable to authenticate "
							+ cred.getUsername()
							+ " with given password using DN: " + dn, e);
		} finally {
			this.closeContext();
		}

		if (acceptedDataUse) {
			return true;
		} else {
			return false;
		}

	}

	public static class DataUseException extends Exception {
		private static final long serialVersionUID = 5255441123074983022L;

		public DataUseException() {
			super("You must agree to the Data Use Terms.");
		}

		public DataUseException(String message) {
			super(message);
		}

		public DataUseException(String message, Exception e) {
			super(message, e);
		}
	};

	public static class AccountUpdateException extends Exception {
		private static final long serialVersionUID = 5513045836496734644L;

		public AccountUpdateException(String message) {
			super(message);
		}

		public AccountUpdateException(String message, Exception e) {
			super(message, e);
		}
	};

	public static class RegistrationException extends Exception {
		private static final long serialVersionUID = 2497202898288855021L;

		public RegistrationException(String message) {
			super(message);
		}

		public RegistrationException(String message, Exception e) {
			super(message, e);
		}
	}

}
