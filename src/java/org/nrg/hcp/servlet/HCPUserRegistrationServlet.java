// Copyright 2012 Washington University School of Medicine All Rights Reserved
package org.nrg.hcp.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import net.tanesha.recaptcha.ReCaptchaImpl;
//import net.tanesha.recaptcha.ReCaptchaResponse;

import org.apache.log4j.Logger;
import org.nrg.hcp.security.HcpLdapAuthenticator;
import org.nrg.xdat.security.Authenticator;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.email.EmailUtils;
import org.nrg.xft.email.EmailerI;
/**
 * @author MRH
 */
@SuppressWarnings("serial")
public class HCPUserRegistrationServlet extends HttpServlet {
	
	public static HashMap<String,Long> captchaCache = new HashMap<String,Long>();
	public static final String XDAT_USER_ELEMENT ="xdat:user";
	
	static final String USERNAME_REGEX = "[a-z_][a-z0-9_]{0,30}";
	
	//// Location of captcha properties file containing public/private keys
	//static final String capPropsFile = "WEB-INF/conf/recaptcha.properties";
	//static String capPrivateKey;
	//static String capPublicKey;
	
	static org.apache.log4j.Logger logger = Logger
			.getLogger(HCPUserRegistrationServlet.class);
	
	/*
	 * Commented out for now - currently not using captcha
	public void init() throws ServletException {
		// Initialize captcha keys
		final Properties props = new Properties();
		try {
			props.load(new FileInputStream(getServletContext().getRealPath("/") + capPropsFile));
		} catch (FileNotFoundException e) {
			throw new ServletException("Captcha properties file does not exist",e);
		} catch (IOException e) {
			throw new ServletException("Could not read captcha properties file",e);
		}
		capPublicKey = props.getProperty("PUBLIC_KEY");
		capPrivateKey = props.getProperty("PRIVATE_KEY");
		// Write public key to servlet context for pull into Login template
		getServletContext().setAttribute("capPublicKey", capPublicKey);
	}
	*/
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest
	 * , javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		
		try {
			// Pull parameters and verify input
	        final String username = assignValue(request.getParameter("username"));
	        final String email = assignValue(request.getParameter("email"));
	        final String pw = assignValue(request.getParameter("pw"));
	        final String firstname = assignValue(request.getParameter("firstname"));
	        final String lastname = assignValue(request.getParameter("lastname"));
	        final String pwc = assignValue(request.getParameter("pwc"));
	        final String institution = assignValue(request.getParameter("institution"));
	        final String department = assignValue(request.getParameter("department"));
	        final String duAccepted = assignValue(request.getParameter("agreeToDataUseTerms"));
	        final String wbAccepted = assignValue(request.getParameter("agreeToWBTerms"));
	        final String isWbForm = assignValue(request.getParameter("isWBForm"));
	        // Validate input
	        if (username==null || !username.matches(USERNAME_REGEX)) {
	        	response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	        	response.getWriter().print("The username entered does not meet requirements");
	        	return;
	        	
	        }
	        if (email==null || email.trim().length()<1 || firstname==null || firstname.trim().length()<1  ||
	        		lastname==null || lastname.trim().length()<1  || 
	        		institution==null || institution.trim().length()<1  || pw==null || pw.trim().length()<1) {
	        	response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	        	response.getWriter().print("Please enter a value for all fields.");
	        	return;
	        }
	        if (!pw.equals(pwc)) {
	        	response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	        	response.getWriter().print("Passwords do not match.");
	        	return;
	        }
	        if (!validatePassword(pw)) {
	        	response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	        	response.getWriter().print("Password does not meet minimum complexity requirements.");
	        	return;
	        }
	        //// Removing captcha (2012-05-12, using honeypot)
	        //if (!isValidCaptcha(request)) {
	        //	response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	        //	response.getWriter().print("Captcha answer did not match expected value");
	        //	return;
	        //}
	        // Honeypot field
	        if (department!=null && department.length()>0) { 
	        	response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	        	response.getWriter().print("Invalid form submission.");
	        	return;
	        }
	        // No longer require acceptance of data use terms for ConnectomeDB registration -- Keep in place for workbench
	        //if (duAccepted!=null && !duAccepted.equalsIgnoreCase("Y") && wbAccepted!=null && !wbAccepted.equalsIgnoreCase("Y")) {
	        if (wbAccepted!=null && !wbAccepted.equalsIgnoreCase("Y") && isWbForm.equals("Y")) {
	        	response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	        	response.getWriter().print("You must agree to the Data Use Terms.");
	        	return;
	        }
	        // Submit Registration
	        HcpLdapAuthenticator auth = new HcpLdapAuthenticator();
	        auth.registerNewLdapAccount(username,email,pw,firstname,lastname,institution,duAccepted,wbAccepted);
        	response.setStatus(HttpServletResponse.SC_OK);
	        if (auth.isAutoEnabled()) {
				response.getWriter().print("Registration Accepted");
	        } else {
				response.getWriter().print("Registration accepted.  Your account must be enabled by an administrator before use.");
	        }

	        /*
			// TEMPORARY SECTION (TESTING FOR DB-599 -- issued accessing accouts registered in LDAP via curl)
	        if (!isWbForm.equals("Y")) {
				HcpLdapAuthenticator.RegisterDataUseAcceptance(username,pw);
			}
			// END TEMPORARY SECTION (TESTING FOR DB-599 -- issued accessing accouts registered in LDAP via curl)
	        */

        	// Send notification to administrator 
	        try {
	        	sendNewUserRequestEmailMessage(username,email,auth.isAutoEnabled());
	        } catch (Throwable t) {
	        	// Don't act on any exception here.  Report only LDAP registration exceptions.
	        }
        	// Perform authentication (need row in XNAT database if not auto-enabled)
	        try {
	        	auth.authenticate(new Authenticator.Credentials(email,pw));
	        } catch (Throwable t) {
	        	// Don't act on any exception here.  Report only LDAP registration exceptions.
	        }
        	
		} catch (HcpLdapAuthenticator.RegistrationException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			try {
				response.getWriter().print(e.getMessage());
			} catch (IOException e1) {
				// Do nothing
			}
		} catch (Exception e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			try {
				response.getWriter().print("Could not process request.  This could be due to invalid data or a server issue");
			} catch (IOException e1) {
				// Do nothing
			}
			e.printStackTrace();
		}
		
	}

	private String assignValue(String parameter) {
        if (parameter!=null) {
        	return parameter.trim();
        } else {
        	return null;
        }
	}

	private boolean validatePassword(String pw) {
		//if (pw!=null && pw.trim().length()>=8 && pw.matches("^.*[a-z].*$") && pw.matches("^.*[A-Z].*$") && pw.trim().matches("^.*[^a-zA-Z].*$")) {
		// Per Dan 2012-10-09, just validate length on back-end (at least 5 characters)
		if (pw!=null && pw.trim().length()>=5) {
			return true;
		}
		return false;
	}

	/*
	private boolean isValidCaptcha(HttpServletRequest request) {
		// See if session has recent validated response for session
		if (isCachedCaptcha(request)) {
			return true;
		}
		// Otherwise validate captcha
		final ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
		reCaptcha.setPrivateKey(capPrivateKey);
		final String remoteAddr = request.getRemoteAddr();
		final String challenge = request.getParameter("recaptcha_challenge_field");
		final String uresponse = request.getParameter("recaptcha_response_field");
		final ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(remoteAddr, challenge, uresponse);
		if (reCaptchaResponse.isValid()) {
		  	captchaCache.put(request.getSession().getId(),System.currentTimeMillis());
		   	return true;
		} else {
		   	return false;
		}
	}
	*/
	
    // See if session has recent correct captcha (To get around fact that reCaptcha can only accept captchas once) 
    private boolean isCachedCaptcha(HttpServletRequest request) {
    	for (Map.Entry<String, Long> entry : captchaCache.entrySet()) {
    		long currentTime = System.currentTimeMillis();
    		if ((currentTime-entry.getValue().longValue())>120000) {
    			captchaCache.remove(entry.getKey());
    		} else  if (entry.getKey().equals(request.getSession().getId())) {
    			return true;
    		}
    	}
		return false;
	}

	public static void sendNewUserRequestEmailMessage(String userName,String email,boolean enabled) {
       String msgBody = getNewUserRequestEmailBody(userName,email,enabled);
       try {
       	EmailerI sm = EmailUtils.getEmailer();
           sm.setFrom(AdminUtils.getAdminEmailId());
           InternetAddress ia = new InternetAddress(AdminUtils.getAdminEmailId());
           ArrayList<InternetAddress> al = new ArrayList<InternetAddress>();
           al.add(ia);
           sm.setTo(al);
           if (enabled) {
        	   sm.setSubject(TurbineUtils.GetSystemName() +" New User Request: " + userName);
           } else {
        	   sm.setSubject(TurbineUtils.GetSystemName() +" New User: " + userName);
           }
           sm.setMsg(msgBody);
           
           sm.send();
       } catch (Exception e) {
               logger.error("Unable to send mail",e);
               System.out.println("Error sending Email");
       }
    }
    
    public static String getNewUserRequestEmailBody(String userName,String email,boolean enabled)
    {
        String msg = "";
        msg +="<html><body>";
        msg +="<b>New User Request</b><br><br>";
        Date d = Calendar.getInstance().getTime();
        msg +="<b>Date:</b> " + d + "<br>";
        msg +="<b>Site:</b> " + TurbineUtils.GetSystemName() + "<br>";
        msg +="<b>Host:</b> " + TurbineUtils.GetFullServerPath() + "<br>";
        msg +="<b>Username:</b> " + userName + "<br>";
        msg +="<b>E-Mail:</b> " + email + "<br>";
        if (enabled) {
        	msg +="This user's account has been created and enabled.  This message is sent for notification purposes only.<br><br><br>";
        } else {
        	msg +="This user's account has been created but will be disabled until you enable the account.<br><br><br>";
        }
       	msg +="<a href=\"" + TurbineUtils.GetFullServerPath() + "/app/action/DisplayItemAction/search_value/" + userName + "/search_element/xdat:user/search_field/xdat:user.login\">";
        if (enabled) {
       	    msg +="Review Account</a><br><br><br>";
        } else {
       	    msg +="Review and Enable</a><br><br><br>";
        }
        msg +="</body></html>";
        return msg;
    }
    
}
