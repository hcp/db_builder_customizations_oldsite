/**
 * Copyright 2011 Washington University
 */
package org.nrg.xnat.restlet.extensions;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.nrg.action.ClientException;
import org.nrg.hcp.restlet.services.utils.BasicParameterHandler;
import org.nrg.hcp.restlet.services.utils.IgnoreParameterHandler;
import org.nrg.hcp.restlet.services.utils.ParameterHandler;
import org.nrg.xdat.collections.DisplayFieldWrapperCollection;
import org.nrg.xdat.om.XdatStoredSearch;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.search.CriteriaCollection;
import org.nrg.xdat.search.DisplaySearch;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.XFT;
import org.nrg.xft.XFTTableI;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.FileRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.io.ByteStreams;

/**
 * Computes an averaged connectome for subjects in the provided stored search.
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
@XnatRestlet("/services/cifti-average")
public final class CIFTIAverage extends SecureResource {
    public static final String SEARCH_ID_KEY = "searchID";
    public static final String CONN_TYPE_KEY = "type";
    public static final MediaType CIFTI_AVERAGE = new MediaType("application/x-caret-cifti-average");
    public static final MediaType CIFTI_METADATA = new MediaType("application/x-caret-cifti-metadata");
    private static final String CARET6_PROPS = "caret6.properties";
    private static final String EXECUTABLE_PROPERTY = "caret6.executable";
    private static final String N_THREADS_PROPERTY = "caret6.threads";
    private static final String DEFAULT_CARET_COMMAND = "/opt/caret6/bin_linux/caret6_command";

    private static final Map<String,ParameterHandler> paramHandlers = new ImmutableMap.Builder<String,ParameterHandler>()
    .put("searchID", new IgnoreParameterHandler())
    .put("type", new IgnoreParameterHandler())
    .put("metadata", new IgnoreParameterHandler())
    .put("column-index", new BasicParameterHandler("-select-column-index"))
    .put("column-surface-node", new BasicParameterHandler("-select-column-surface-node", ",", 2))
    .put("column-voxel-ijk", new BasicParameterHandler("-select-column-voxel-ijk", ",", 4))
    .put("column-voxel-xyz", new BasicParameterHandler("-select-column-voxel-xyz", ",", 3))
    .put("row-index", new BasicParameterHandler("-select-row-index"))
    .put("row-surface-node", new BasicParameterHandler("-select-row-surface-node", ",", 2))
    .put("row-voxel-ijk", new BasicParameterHandler("-select-row-voxel-ijk", ",", 4))
    .put("row-voxel-xyz", new BasicParameterHandler("-select-row-voxel-xyz", ",", 3))
    .build();

    private static enum ConnectomeType {
        dconn, dtseries
    };

    private static final Map<ConnectomeType,String> connectomeNames =
        ImmutableMap.of(ConnectomeType.dconn, "Run1.dconn.nii", ConnectomeType.dtseries, "Run1_tcs.dtseries.nii");

    private final Logger logger = LoggerFactory.getLogger(CIFTIAverage.class);
    private final Multimap<String,Object> params = LinkedHashMultimap.create();
    private final String executable;
    private final int nThreads;

    public CIFTIAverage(final Context context, final Request request, final Response response) {
        super(context, request, response);
        final Properties properties = getProperties();
        executable = properties.getProperty(EXECUTABLE_PROPERTY, DEFAULT_CARET_COMMAND);
        nThreads = Integer.valueOf(properties.getProperty(N_THREADS_PROPERTY, "1"));
    }

    @Override
    public boolean allowGet() { return true; }

    @Override
    public boolean allowPost() { return true; }

    @Override
    public void handleGet() {
        final StringBuilder sb = new StringBuilder("<html>");
        sb.append("<h2>CIFTI averaging service</h2>");
        sb.append("<p>POST with query parameters to use this service.</p>");
        sb.append("<h3>query parameters:</h3>");
        for (final String key : paramHandlers.keySet()) {
            sb.append("<p>").append(key).append("</p>");
        }
        sb.append("</html>");
        this.getResponse().setEntity(sb.toString(), MediaType.TEXT_HTML);
    }

    @Override
    public void handleParam(final String key, final Object value) {
        params.put(key, value);
    }

    @Override
    public void handlePost() {
        try {
        	loadQueryVariables();
        } catch (ClientException e) {
            logger.error("unable to load query parameters", e);
            this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST, "unable to parse query parameters: " + e.getMessage());
            return;
        }

        final List<File> files;
        try {
            files = getCIFTIFiles(user, params.get(SEARCH_ID_KEY), params.get(CONN_TYPE_KEY));
        } catch (Throwable t) {
            logger.error("unable to perform search", t);
            this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, "unable to perform search: " + t.getMessage());
            return;
        }

        if (files.isEmpty()) {
            logger.info("search {} did not produce any CIFTI files", params.get(SEARCH_ID_KEY));
            this.getResponse().setStatus(Status.SUCCESS_NO_CONTENT);
            return;
        }

        final List<String> args = Lists.newArrayList(executable, "-cifti-average");
        args.add("-number-of-threads");
        args.add(Integer.toString(nThreads));
        try {
            mapArguments(args, params);
        } catch (IllegalArgumentException e) {
            logger.error("error parsing service argument", e);
            this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST, e.getMessage());
            return;
        }

        final MediaType mediaType;
        File output = null;
        try {
            if (params.containsKey("metadata")) {
                output = File.createTempFile("cifti-metadata", "txt");
                args.add("-output-text-file");
                args.add(output.getPath());
                args.add("-select-metadata");
                args.add(files.get(0).getPath());
                mediaType = CIFTI_METADATA;
            } else {
                output = File.createTempFile("cifti-average", "");
                args.add("-output-binary-file");
                args.add(output.getPath());
                args.add("-share-metadata-from-first-cifti-file");
                for (final File f : files) {
                    args.add(f.getPath());
                }
                mediaType = CIFTI_AVERAGE;
            }
        } catch (IOException e) {
            logger.error("unable to create temp file", e);
            this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, e.getMessage());
            return;
        }

        try {
            logger.trace("executing caret6: {}", args);
            final Process process = Runtime.getRuntime().exec(args.toArray(new String[0]));
            final StreamConsumer stdout = new StreamConsumer(process.getInputStream()).start(),
            stderr = new StreamConsumer(process.getErrorStream()).start();
            final int status = process.waitFor();
            if (0 != status) {
                logger.error("CIFTI averaging failed: error code {}", status);
                this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,
                        "CIFTI averaging failed, error code " + status + ": " + stderr);
            }
            this.getResponse().setEntity(new FileRepresentation(output, mediaType, 0));
        } catch (IOException e) {
            logger.error("error running averaging process", e);
            this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, "error computing CIFTI average");
            return;
        } catch (InterruptedException e) {
            logger.error("CIFTI averaging interrupted", e);
            this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, "CIFTI averaging interrupted: " + e.getMessage());
            return;
        }
    }


    private static final List<String> mapArguments(final List<String> args, final Multimap<String,Object> params) {
        for (final String key : params.keySet()) {
            final ParameterHandler handler = paramHandlers.get(key);
            if (null != handler) {
                handler.unpack(key, params.get(key), args);
            } else {
                slog().warn("no parameter handler defined for {}", key);
            }
        }
        return args;
    }

    private static final Logger slog() {
        return LoggerFactory.getLogger(CIFTIAverage.class);
    }

    private static final List<File> getCIFTIFiles(final XDATUser user, final Iterable<?> searchIDs, final Iterable<?> types)
    throws Exception {
        final Logger logger = slog();
        final List<File> files = Lists.newArrayList();

        final ConnectomeType type;
        if (null == types) {
            throw new IllegalArgumentException("missing required parameter \"type\"");
        } else {
            final Iterator<?> typei = types.iterator();
            if (!typei.hasNext()) {
                throw new IllegalArgumentException("parameter \"type\" must be set");
            } else {
                final String name = typei.next().toString();
                try {
                    type = ConnectomeType.valueOf(name);
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException("unknown connectome type \"" + name + "\"");
                }
                if (typei.hasNext()) {
                    throw new IllegalArgumentException("parameter \"type\" must have one value");
                }
            }
        }
        for (final Object searchID : searchIDs) {
            logger.trace("using search {} for CIFTI average", searchID);
            final XdatStoredSearch stored = XdatStoredSearch.getXdatStoredSearchsById(searchID, user, true);
            final String rootName = stored.getRootElementName();
            if ("xnat:mrSessionData".equalsIgnoreCase(rootName)) {
                addCIFTIFilesForMRSessions(files, user, stored.getDisplaySearch(user), type);
            } else if ("xnat:subjectData".equalsIgnoreCase(rootName)) {
                addCIFTIFilesForSubjects(files, user, stored.getDisplaySearch(user), type);
            } else {
                logger.debug("ignoring unimplemented search root " + stored.getRootElementName());
            }
        }
        return files;
    }

    private static final <T extends Collection<? super File>>
    T addCIFTIFilesForSubjects(final T files, final XDATUser user, final DisplaySearch subjectSearch, final ConnectomeType type)
    throws Exception {
        final Logger logger = slog();
        subjectSearch.setFields(new DisplayFieldWrapperCollection());
        subjectSearch.addDisplayField("xnat:subjectData", "SUBJECT_ID");
        final XFTTableI table = subjectSearch.execute(user.getLogin());
        if (null == table) {
            logger.info("subject data search {} returned null result", subjectSearch);
            return files;
        }
        table.resetRowCursor();
        final DisplaySearch sessions = new DisplaySearch();
        sessions.setUser(user);
        sessions.setRootElement(XnatMrsessiondata.SCHEMA_ELEMENT_NAME);
        sessions.addDisplayField("xnat:mrSessionData", "PROJECT");
        sessions.addDisplayField("xnat:mrSessionData", "LABEL");

        final CriteriaCollection cc= new CriteriaCollection("OR");
        while (table.hasMoreRows()) {
            final Map<?,?> row = table.nextRowHash();
            logger.trace("subject search found {}", row);
            cc.addClause("xnat:mrSessionData/subject_ID", row.get("subject_id").toString());
        }
        sessions.addCriteria(cc);

        final XFTTableI st = sessions.execute(user.getLogin());
        if (null == st) {
            logger.info("subject sessions search {} returned null result", sessions);
            return files;
        }
        st.resetRowCursor();
        while (st.hasMoreRows()) {
            final Map<?,?> row = st.nextRowHash();
            logger.trace("session search found {}", row);
            addCIFTIFiles(files, row.get("project").toString(), row.get("label").toString(), type);
        }
        return files;
    }

    private static final <T extends Collection<? super File>>
    T addCIFTIFilesForMRSessions(final T files, final XDATUser user,
            final DisplaySearch mrSessionSearch, final ConnectomeType type)
    throws Exception {
        final Logger logger = slog();
        mrSessionSearch.setFields(new DisplayFieldWrapperCollection());
        mrSessionSearch.addDisplayField("xnat:mrSessionData", "PROJECT");
        mrSessionSearch.addDisplayField("xnat:mrSessionData", "LABEL");
        final XFTTableI table = mrSessionSearch.execute(user.getLogin());
        if (null == table) {
            logger.info("sessions search {} returned null result", mrSessionSearch);
            return files;
        }
        table.resetRowCursor();
        while (table.hasMoreRows()) {
            final Map<?,?> row = table.nextRowHash();
            logger.trace("session search found {}", row);
            addCIFTIFiles(files, row.get("project").toString(), row.get("label").toString(), type);
        }
        return files;
    }

    private static final <T extends Collection<? super File>>
    T addCIFTIFiles(final T files, final String project, final String session, final ConnectomeType type) {
        final File rootArchivePath = new File(XFT.GetArchiveRootPath());
        final File projdir = new File(rootArchivePath, project);
        final File arc001 = new File(projdir, "arc001");
        final File sessdir = new File(arc001, session);
        addCIFTIFiles(files, sessdir, type);
        return files;
    }

    private static final <T extends Collection<? super File>>
    T addCIFTIFiles(final T files, final File sessionRoot, final ConnectomeType type) {
        final File resultsDir = new File(sessionRoot, "RESOURCES/MPRAGE_SURFACE_REG/caret/Results");
        final File ciftiFile = new File(resultsDir, connectomeNames.get(type));
        if (ciftiFile.canRead()) {
            files.add(ciftiFile);
        }
        return files;
    }

    private static final class StreamConsumer implements Runnable {
        private final InputStream in;
        private final ByteArrayOutputStream out = new ByteArrayOutputStream();

        StreamConsumer(final InputStream in) {
            this.in = in;
        }

        public void run() {
            final Logger logger = LoggerFactory.getLogger(StreamConsumer.class);
            try {
                ByteStreams.copy(in, out);
            } catch (IOException e) {
                logger.error("error consuming stream", e);
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                    logger.error("error closing stream", e);
                }
            }
        }

        public StreamConsumer start() {
            new Thread(this).start();
            return this;
        }

        @Override
        public String toString() {
            return out.toString();
        }
    }

    public static Properties getProperties() {
        final File propsfile = new File(XFT.GetConfDir(), CARET6_PROPS);
        final Properties properties = new Properties();
        try {
            properties.load(new FileReader(propsfile));
        } catch (IOException e) {
            slog().debug("caret6 properties " + propsfile + " could not be read", e);
        }
        return properties;
    }
}
