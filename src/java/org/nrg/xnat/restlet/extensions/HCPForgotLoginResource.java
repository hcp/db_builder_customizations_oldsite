/**
 * Copyright 2011 Washington University
 */
package org.nrg.xnat.restlet.extensions;

import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

import javax.mail.MessagingException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xdat.entities.AliasToken;
import org.nrg.xdat.services.AliasTokenService;

import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.util.RequestUtil;
import org.restlet.Context;
import org.restlet.resource.Representation;
import org.restlet.resource.Resource;
import org.restlet.resource.StringRepresentation;
import org.restlet.data.Form;
import org.restlet.data.Parameter;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;

import com.google.common.collect.Maps;

/**
 * Computes an averaged connectome for subjects in the provided stored search.
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
@XnatRestlet(value = "/services/forgotlogin",secure = false)
public final class HCPForgotLoginResource extends Resource {
    static Logger logger = Logger.getLogger(HCPForgotLoginResource.class);

	public static final String EMAIL_KEY = "email";
	public static final String ACCOUNT_KEY = "account";

	public HCPForgotLoginResource(final Context context, final Request request, final Response response) {
		super(context, request, response);
	}

	@Override
	public boolean allowGet() { return true; }

	@Override
	public boolean allowPost() { return true; }

	@Override
	public boolean allowPut() { return false; }

	@Override
	public boolean allowDelete() { return false; }

	@Override
	public void handleGet() {
		handlePost();
	}

	@Override
	public void handlePost() {
		
		String email = this.getQueryVariable(EMAIL_KEY);
		String username = this.getQueryVariable(ACCOUNT_KEY);
		String subject = TurbineUtils.GetSystemName() + " Login Request";
		String admin = AdminUtils.getAdminEmailId();
		
		try {
		
		if (!StringUtils.isBlank(username)) {
            //check user
                ItemSearch search = new ItemSearch();
                search.setAllowMultiples(false);
                search.setElement("xdat:user");
                search.addCriteria("xdat:user.login",username);

                ItemI temp = search.exec().getFirst();
                if (temp==null){
                	this.returnString("Unknown username.", Status.CLIENT_ERROR_BAD_REQUEST);
                    return;
                }else{
                	XDATUser user = new XDATUser(temp, false);

                    try {
                    	String to = user.getEmail();
				        String url=TurbineUtils.GetFullServerPath() + "/app/action/XDATActionRouter/xdataction/MyXNAT";
				        AliasToken token = XDAT.getContextService().getBean(AliasTokenService.class).issueTokenForUser(user,true,null);
				        String text = "Dear " + user.getFirstname() + " " + user.getLastname() + ",<br/>\r\n" + "Please click this link to reset your password: " + TurbineUtils.GetFullServerPath() + "/app/template/ChangePassword.vm?a=" + token.getAlias() + "&s=" + token.getSecret() + "<br/>\r\nThis link will expire in 24 hours.";
				        XDAT.getMailService().sendHtmlMessage(admin, to, subject, text);
				        this.returnString("You have been sent an email with a link to reset your password. Please check your email.",Status.SUCCESS_OK);
                        return;
				        //data.setScreenTemplate("Login.vm");
				        //data.setScreen("/app/template/Login.vm");
                    } catch (MessagingException e) {
                        logger.error("Unable to send mail",e);
                        System.out.println("Error sending Email");

                        this.returnString("Due to a technical difficulty, we are unable to send you the email containing your information.  Please contact our technical support.",Status.SERVER_ERROR_INTERNAL);
                        return;
                    }
					
                }
		
		} else if (!StringUtils.isBlank(email)) {
            //check email
            ItemSearch search = new ItemSearch();
            search.setAllowMultiples(false);
            search.setElement("xdat:user");
            search.addCriteria("xdat:user.email",email);

            ItemI temp = search.exec().getFirst();
            if (temp==null){
                this.returnString("Unknown email address.",Status.CLIENT_ERROR_BAD_REQUEST);
                return;
            }else{
				XDATUser user = new XDATUser(temp, false);
                
                try {
                	String url=TurbineUtils.GetFullServerPath() + "/app/template/Index.vm";
                    String message = String.format(USERNAME_REQUEST, user.getUsername(), url, TurbineUtils.GetSystemName());
                    XDAT.getMailService().sendHtmlMessage(admin, email, subject, message);
					this.returnString("The corresponding username for this email address has been emailed to your account.",Status.SUCCESS_OK);
					return;
				} catch (MessagingException exception) {
					logger.error(exception);
					this.returnString("Due to a technical difficulty, we are unable to send you the email containing your information.  Please contact our technical support.",Status.SERVER_ERROR_INTERNAL);
					return;
                }
            }
            
        }else{
           this.returnString("Unknown/Missing username/email address.",Status.CLIENT_ERROR_BAD_REQUEST);
           return;
        }
			
		} catch (Exception e) {
			logger.error(e);
			this.returnString("Due to a technical difficulty, we are unable to process your request.  Please contact our technical support.",Status.SERVER_ERROR_INTERNAL);
			return;
			
		}
		
    }

	// TODO: This should be converted to use a Velocity template or property in
	// a resource bundle.
	private static final String USERNAME_REQUEST = "<html><body>\nYou requested your username, which is: %s\n<br><br><br>Please login to the site for additional user information <a href=\"%s\">%s</a>.\n</body></html>";
	private static final String PASSWORD_RESET = "<html><body>\nYour password has been reset to:<br>%s\n<br><br><br>Please login to the site and create a new password in the <a href=\"%s\">account settings</a>.\n</body></html>";

	private void returnString(String string, Status status) {
		getResponse().setEntity(new StringRepresentation(string));
		getResponse().setStatus(status);
	}
	
	private Form f= null;
	
	private Form getQueryVariableForm(){
		if(f==null){
			f= getRequest().getResourceRef().getQueryAsForm();
		}
		return f;
	}

	public String getQueryVariable(String key){
		Form f = getQueryVariableForm();
		if (f != null && f.getValuesMap().containsKey(key)) {
			return TurbineUtils.escapeParam(f.getFirstValue(key));
		}
		return null;
	}

}

