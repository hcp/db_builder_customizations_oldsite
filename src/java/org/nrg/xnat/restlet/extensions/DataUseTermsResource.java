/**
 * Copyright 2011 Washington University
 */
package org.nrg.xnat.restlet.extensions;

import org.apache.directory.shared.ldap.aci.UserClass.ThisEntry;
import org.nrg.hcp.security.HcpLdapAuthenticator;
import org.nrg.hcp.security.HcpLdapAuthenticator.DataUseException;
import org.nrg.xdat.entities.XDATUserDetails;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;

/**
 * Computes an averaged connectome for subjects in the provided stored search.
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
@XnatRestlet("/services/datause")
public final class DataUseTermsResource extends SecureResource {

	//public static final String USER_KEY = "user";
	//public static final String GROUP_KEY = "group";
	//public static final String PROJECT_KEY = "project";

	public static final String TERMS_KEY = "terms";
	public static final String ACCEPT_KEY = "acceptTerms";
	public enum TERMS { WB , PHASE1 , PHASE2 };

	public DataUseTermsResource(final Context context, final Request request, final Response response) {
		super(context, request, response);
	}

	@Override
	public boolean allowGet() { return true; }

	@Override
	public boolean allowPost() { return true; }

	@Override
	public boolean allowPut() { return true; }

	@Override
	public boolean allowDelete() { return true; }

	@Override
	public void handleGet() {
		String termsP = this.getQueryVariable(TERMS_KEY);
		try {
			String authUser = ((XDATUserDetails) user).getAuthorization().getAuthUser();
			this.returnString(String.valueOf(HcpLdapAuthenticator.AcceptedDataUse(authUser,termsP)),Status.SUCCESS_OK);
		} catch (DataUseException e) {
			this.getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"Processing exception likely due to invalid value for \"terms\".");
		}
	}

	@Override
	public void handlePost() {
		String termsP = this.getQueryVariable(TERMS_KEY);
		String acceptP = this.getQueryVariable(ACCEPT_KEY);
		// In the unlikely case users were to make direct REST calls, use acceptTerms to require acknowledgment of acceptance
		if (acceptP==null || !acceptP.equalsIgnoreCase("true")) {
			this.getResponse().setStatus(Status.CLIENT_ERROR_NOT_ACCEPTABLE,"PUT/POST requests must pass " + ACCEPT_KEY + " parameter (" + ACCEPT_KEY + "=true).");
			return;
		}
		try {
			String authUser = ((XDATUserDetails) user).getAuthorization().getAuthUser();
			this.returnString(String.valueOf(HcpLdapAuthenticator.RecordDataUseAcceptance(authUser,termsP)),Status.SUCCESS_OK);
		} catch (DataUseException e) {
			this.getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"Processing exception likely due to invalid value for \"terms\".");
		}
	}

	@Override
	public void handlePut() {
		handlePost();
	}

	@Override
	public void handleDelete() {
		String termsP = this.getQueryVariable(TERMS_KEY);
		try {
			String authUser = ((XDATUserDetails) user).getAuthorization().getAuthUser();
			this.returnString(String.valueOf(HcpLdapAuthenticator.RemoveDataUseAcceptance(authUser,termsP)),Status.SUCCESS_OK);
		} catch (DataUseException e) {
			this.getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"Processing exception likely due to invalid value for \"terms\".");
		}
	}

}

