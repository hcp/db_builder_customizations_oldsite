<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Data | Human Connectome Project</title>
<meta http-equiv="Description" content="Public access to HCP data. Currently, Phase 1 Pilot Data is available." />
<meta http-equiv="Keywords" content="Public data release, Human Connectome Project, pilot data, brain connectivity data" />
<link href="$content.getURI('/style/hcpv3.css')" rel="stylesheet" type="text/css" />
<link href="$content.getURI('/style/data.css')" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="$content.getURI('/scripts/global.js')"></script>
<script type="text/javascript" src="$content.getURI('/scripts/jquery/jquery-1.7.2.min.js')"></script> 
<script type="text/javascript" src="$content.getURI('/scripts/jquery.easing.1.2.js')"></script>

<script type="text/javascript">
// modified URL parser from http://papermashup.com/read-url-get-variables-withjavascript/
function getUrlVars() {
    // look for variables after the URL. Remove the URL itself.
	var parts = window.location.href.split("?");
	// parts=parts.shift();
    return parts[1];
}
</script>

</head>

<body>

<!-- begin masked content for overlay -->
<div id="page-mask" class="pm" onclick="javascript:modalClose();"></div>

<script type="text/javascript" src="/scripts/data-access.js"></script>

<!-- end masked content for overlay -->
<!-- site container -->
<div id="site_container">

<div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
</div><!-- #BeginLibraryItem "/Library/ConnectomeDB header.lbi" --><!-- Header -->
<div id="header"><img src="/images/img/header-bg-connectomedb.png" alt="Human Connectome Project | Creating a complete map of structure and function in the human brain." border="0" usemap="#Header" />
  <map name="Header" id="Header">
    <area shape="rect" coords="14,7,250,123" href="$content.getURI('/')" alt="Human Connectome Project Logo" />
  </map>
</div> 
<!-- end Header -->

<!-- top level navigation --><div id="topnav">
<ul id="nav">
	<li><a href="$content.getURI('/')">DB Home</a></li>
    <li><a href="http://humanconnectome.org/connectome/connectomeDB.html" target="_new">About ConnectomeDB</a>
    </li>
    <li><a href="http://humanconnectome.org/documentation/data-release/" target="_new">Documentation</a>
    	<ul>
        	<li><a href="http://humanconnectome.org/documentation/data-release/ftp-instructions.html" target="_new">FTP Download Instructions</a></li>
            <li><a href="http://humanconnectome.org/data/faq/" target="_new">How to Access Data</a></li>
        </ul>
    </li> 
    <li style="float:right; margin-right:30px;">
    	<a href="http://humanconnectome.org/data/"><img src="/images/img/arrow-right.png" style="margin-right:10px;" border="0" />Back to HCP Site</a>
    </li>
  </ul>
</div><!-- End Nav --><!-- #EndLibraryItem --><!-- banner -->
<div id="bannerOpen" class="data-banner" style="height:310px;">
  <h1>Edit Account Info</h1>
  <div id="user_info">Logged in as: <strong>$!data.getSession().getAttribute("user").getUsername()</strong><a href="$content.getURI('/app/action/LogoutUser')"><input type="button" value="Log Out" /></a></div>
  <div class="overlay-container-half">
      <div class="overlay-half-mask"></div>
      <div class="overlay-half">
      	<p><strong>Change Email for User Name ($!data.getSession().getAttribute("user").getUsername())</strong></p>
        <form name="edit-email" class="friendlyForm" method="post" action="$!link.setAction("HCPModifyEmail")" onsubmit="return ConfirmEmail();" _lpchecked="1">
			<p style="margin-bottom:26px;">
				<label>Current Email:</label> <input type="text" value="$!item.getProperty("email")" disabled="disabled" />
            </p>
			<p style="margin-bottom:26px;">
				<label for="new_email">New Email:</label> <input id="new_email" type="text" name="xdat:user.email" value="">

            </p>
            <p id="email-error" class="error hidden" style="margin-top:-9px;margin-bottom:0px;background-color:#FFDDDD">Improper email entered.</p>
            <p style="margin-bottom:26px;">
            	<label for="confirm_email">Confirm  Email: </label> 
            	<input id="confirm_email" type="text" name="confirm_email" value="">
            </p>
            <input type="hidden" name="destination" value="Index.vm">
            <input type="hidden" name="message" value="Email Changed.">
	#xdatHiddenBox("xdat:user.xdat_user_id" $item "")
	#xdatHiddenBox("xdat:user.login" $item "")
	#xdatEditProps($item $edit_screen)
            <p class="form-submit"><input type="submit" id="email_submit" name="eventSubmit_doSetup" value="Submit" disabled="disabled"></p>
        </form>
     </div>
     
     <div class="overlay-half-mask" style="right:0; width: 470px;"></div>
      <div class="overlay-half" style="right:0; width:440px;">
#parse("/screens/userValidationJS.vm")
      	<p><strong>Change Password for User Name ($!data.getSession().getAttribute("user").getUsername())</strong></p>
        <form name="change-password" class="friendlyForm"  method="post" action="$!link.setAction("HCPModifyPassword")" onsubmit="if(ConfirmPassword() &amp;&amp; validatePassword(document.getElementById('xdat:user.primary_password')))return true;else return false;">
			<p style="margin-bottom:26px;">
            	<label for="new_password">New Password:</label> <input id="new_password" type="password" name="xdat:user.primary_password" value="" autocomplete="off" style="left:150px;" />
            </p>
	    <p class="error hidden" id="password-simple" style="margin-top:-9px;margin-bottom:0px;background-color:#FFDDDD">Password must be 5 or more characters.</p>
			<p style="margin-bottom:26px;">
            	<label for="confirm_password">Confirm Password:</label> <input id="confirm_password" type="password" name="confirm_password" value="" autocomplete="off" style="left:150px;" />
            </p>
	#xdatHiddenBox("xdat:user.xdat_user_id" $item "")
	#xdatHiddenBox("xdat:user.login" $item "")
	#xdatEditProps($item $edit_screen)
			<p class="form-submit"><input type="submit" id="pw_submit" name="eventSubmit_doSetup" value="Submit" disabled="disabled"></p>
        </form>
     </div>
  </div>
  
</div>

<!-- end banner -->

<!-- "Middle" area contains all content between the top navigation and the footer -->
<div id="middle">
  <div id="content">
		
        <p>&laquo; Back to <a href="$content.getURI('/')">Data Home Page</a></p>
	
  </div>
  <!-- /#Content -->
  
</div> <!-- middle -->

<div id="footer"><!-- #BeginLibraryItem "/Library/Footer text.lbi" -->
<p style="font: 12px/20px Arial, Helvetica, sans-serif; text-align:justify;"><span style="font: 18px Georgia, 'Times New Roman', Times, serif"><a href="http://www.wustl.edu/" title="Washington University in Saint Louis" target="_new">Washington University in Saint Louis</a> - <a href="http://www.umn.edu/" title="University of Minnesota" target="_new">University of Minnesota</a></span><br />
<a href="http://www.ox.ac.uk/" title="Oxford University" target="_new">Oxford University</a> - <a href="http://www.slu.edu/" title="Saint Louis University (SLU)" target="_new">Saint Louis University</a> - <a href="http://www.advancedmri.com/" title="Advanced MRI Technologies (AMRIT)" target="_new">Advanced MRI Technologies</a> - <a href="http://www.iu.edu/" title="Indiana University" target="_new">Indiana University</a> - <a href="http://www.unich.it/unichieti/appmanager/unich_en/university_en?_nfpb=true&_pageLabel=P15800195411249373765813" title="University d'Annunzio" target="_new">University d&apos;Annunzio</a><br />
<a href="http://www.esi-frankfurt.de/" title="Ernst Strungmann Institute" target="_new">Ernst Strungmann Institute</a> - <a href="http://www2.warwick.ac.uk/" title="Warwick University" target="_new">Warwick University</a> - <a href="http://berkeley.edu/" title="University of California at Berkeley" target="_new">University of California at Berkeley</a> - <a href="http://www.ru.nl/donders/" title="The Donders Institute" target="_new">Radboud University Nijmegen</a></p>
<p>The Human Connectome Project is funded by the <a href="http://www.nih.gov/" title="National Institutes of Health (NIH)" target="_new">National Institutes of Health</a>, and all information in this site is available to the public domain. No Protected Health Information has been published on this site. Last updated <script type="text/javascript">document.write(document.lastModified);</script>. 
</p>

<p><a href="http://humanconnectome.org/privacy/">Privacy</a> |  <a href="http://humanconnectome.org/sitemap/">Site Map</a> | <a href="http://humanconnectome.org/consortia/" style="color:#33c; font-weight:bold">NIH Blueprint for Neuroscience Research: the Human Connectome Project</a> | Follow <a href="http://twitter.com/HumanConnectome"><strong>@HumanConnectome</strong></a> on Twitter</p> 

<!-- #EndLibraryItem --></div>

</div>
<!-- end container -->

</body>
</html>
#*<A href="$link.setAction("XDATActionRouter").addPathInfo("xdataction","password")">Change Password</a><br>*#
#*<A href="$link.setAction("XDATActionRouter").addPathInfo("xdataction","change_email")">Change Email</a>*#
